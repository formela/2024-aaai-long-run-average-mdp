# Optimizing Local Satisfaction of Long-Run Average Objectives in Markov Decision Processes (AAAI 2024)

Long-run average optimization problems for Markov decision processes (MDPs) require constructing policies with optimal steady-state behavior, i.e., optimal limit frequency of visits to the states. However, such policies may suffer from local instability in the sense that the frequency of states visited in a bounded time horizon along a run differs significantly from the limit frequency. In this work, we propose an efficient algorithmic solution to this problem.

Presented on [AAAI 2024](https://aaai.org/aaai-conference/).

Klaška, D., Kučera, A., Kůr, V., Musil, V., & Řehák, V. (2024). Optimizing Local Satisfaction of Long-Run Average Objectives in Markov Decision Processes. Proceedings of the AAAI Conference on Artificial Intelligence, 38(18), 20143-20150. https://doi.org/10.1609/aaai.v38i18.29993

The arXiv version of the paper is available [here](https://arxiv.org/abs/2312.12325).

## Preparation

Create virtual environment and install all packages:

1. Make sure you have `python3` and `pipenv` installed. For the up-to-date version, run

```shell
python3 -m pip install --upgrade pip
python3 -m pip install pipenv
cd /the-main-directory-of-src/
python3 -m pipenv --rm
```

2. Run `python3 -m pipenv install` (at your own risk with `--skip-lock`)
3. Run the environment `python3 -m pipenv shell`
4. Install torch package  `python -m pip install torch`
5. Install ccp_extensions by `make build_cpp`

## Examples

```shell
python3 -m src +experiment=stability/fifths epochs=1 trainer.steps=100
python3 -m src +experiment=stability/path_6 epochs=1 trainer.steps=800 +model.objective.beta=0.0 +model.objective.gamma=0.2
```

## Optimization

Run the optimization (in the above installed pipenv shell) by

```shell
python3 -m srct +experiment=<path/to/your/settings> model_params.global_memory=3 reporting=mongo
```

where

- `path/to/your/settings` is the path to your configuration file inside of `conf/experiment`.
- reporting can be `disk` or `mongo` (see `conf/config.yaml:io_devices`)
- adjust `conf/logging/custom.yaml` to configure:
  - format of the logs
  - handlers: disk, console, mongo (disabled by default)
- override any other settings by `<setting>=<value>` similarly to `model_params.global_memory=3` (see `conf/config.yaml`)

Alternatively, run

```shell
python3 -m srct cmd=train_parallel
```

which runs more optimizations in parallel with `ray` library (NOT WORKING).

The configuration is based on `OmegaConf` and `hydra` packages.
For more details, see `conf/config.yaml` and the rest of the config directory.

## Results

The results can be saved locally to `disk` or to a `MongoDB`.

### Disk (default)

Enabled by default or by appending `reporting=disk` to the command line.

The results, visualizations and logs are stored int the configured directory, including

- `stats_epoch.csv` Detailed statistics for all epochs
- `stats_final.csv` Statistics summary over all epochs
- `stats_full.csv` Detailed statistics for all steps of all epochs
- `times_full.csv` Time statistics for all steps of all epochs

and optionally also:

- `check_points` Directory with exports of all torch parameter values
- `strategies` Directory with optimized strategies exported as numpy matrices
- `plots` Directory with plots:
  - `avg_times.pdf` Plot of average runtimes ("forward", "backward", and "other" times)
  - `training_progress.pdf` Plots of strategy values during optimization
