# Standard Imports
from typing import Union, Optional, List
import functools
import os
from time import process_time
from copy import deepcopy

# Third-party Imports
import socket
import git

# Local Imports
from src.io.logging import get_logger
from src.io.writers import WritersDict


logger = get_logger('reporting')

mode_dct = {
    'min': -1,
    'max': 1
}


def get_git_commit_hash(path=None):
    if path is None:
        path = os.getcwd()
    try:
        repo = git.Repo(path)
    except git.InvalidGitRepositoryError or git.exc.NoSuchPathError:
        return None

    return repo.head.reference.commit.hexsha[:7]


class ExperimentReporting(object):

    def __init__(
            self,
            mode: str,
            writers: dict,
            project: str,
            tag: Optional[Union[str, List]] = None,
            description: Optional[str] = None,
            commit: Optional[str] = None,
            aggregations: Optional[List] = None
    ) -> None:

        self.writer = WritersDict(writers)
        self.mode = mode
        self.aggregations = [
            ('metrics_stats.best_val', 'metrics.best_val', self.mode),
            ('metrics_stats.best_val', 'metrics.avg_best_val', 'avg'),
            ('metrics_stats.best_val', 'metrics.std_best_val', 'stdDevPop'),
            ('metrics_stats.avg_val', 'metrics.avg_val', 'avg'),
            # todo: best_val_at_epoch
            # ('metrics.epoch', 'metrics.best_val_at_epoch', 'argmax/argmin'),  #not defined!!
            ('times_stats.total', 'times.avg_epoch', 'avg'),
            ('times_stats.avg_sum', 'times.avg_step', 'avg'),
        ]
        self.aggregations += [(f'times_stats.avg_{x}', f'step_times.avg_{x}', 'avg') for x in
                              ['forward', 'backward', 'optimizer', 'eval']]
        self.aggregations += aggregations or []

        # todo: fix settings reporting
        # report_settings = deepcopy(cfg)
        # try:
        #     del report_settings['io_devices']['mongo']['client']
        # except KeyError:
        #     pass
        # self.writer.add('settings', report_settings)
        self.writer.add('project', project)
        self.writer.add('tag', tag)
        self.writer.add('description', description)
        self.writer.add('hostname', socket.gethostname())
        self.writer.add('script', os.path.basename(__file__))
        self.writer.add('commit', commit or get_git_commit_hash())

        self.start_time = process_time()

    def update(self, run):
        self.writer.call_zipped('add_run', run)

    def dump(self):
        self.writer.aggregate_stats(self.aggregations)
        self.writer.add('times.total', process_time() - self.start_time)

        data = self.writer.view()
        _, data = next(iter(data.items()))  # all writers should return the same data, taking the first one
        self.print_stats(data)

        self.writer.close()
        return data.get('metrics')

    def add_graph(self, graph):
        self.writer.add_binary('graph.graph', graph.graph)
        self.writer.add('graph.nodes', [str(n) for n in graph.nodes])
        # TODO: report graph data
        # self.writer.add('graph.aug_nodes', [str(n) for n in graph.aug_nodes])
        # self.writer.add_binary('graph.aug_graph', graph.aug_graph)
        # self.writer.add_binary('graph.aug_node_map', graph.aug_node_map)

    def print_stats(self, data):
        msg = '-------------'
        msg += f"\nbest val\t= {data['metrics']['best_val']:.3f}"
        msg += f"\navg epoch time\t= {data['times']['avg_epoch']:.3f}"
        msg += f"\navg step time\t= {data['times']['avg_step']:.3f}"
        msg += f"\ntotal time\t= {data['times']['total']:.1f}"
        logger.info(msg)

        # todo: print the strategy for best_val
        # self.graph.print_strategy(strategy= ...)

    @property
    def experiment_id(self):
        return self.writer.get_id()


class RunReporting(object):
    def __init__(
            self,
            epoch: int,
            seed: int,
            mode: str,
            experiment_id: Union[str, dict],
            writers: dict[str: functools.partial],
            tag: Optional[Union[List, str]] = None,
            aggregations: Optional[List] = None
    ) -> None:
        epoch += 1
        writers = {k: v(epoch=epoch) for k, v in writers.items()}
        self.writer = WritersDict(writers)
        self.mode = mode
        self.writer.add('seed', seed)
        self.writer.add('tag', tag)
        experiment = {k: 'experiment' for k, _ in experiment_id.items()}
        self.writer.call_zipped('add', experiment, experiment_id)
        self.writer.add('epoch', epoch)

        self.best_val = - mode_dct[self.mode] * float('inf')
        self.best_at = 0
        self.extras = set()
        self.aggregations = [
            ('metrics.val', 'metrics_stats.end_val', 'last'),
            ('metrics.val', 'metrics_stats.avg_val', 'avg'),
        ]
        self.aggregations += [(f'times.{x}', f'times_stats.avg_{x}', 'avg') for x in
                              ['forward', 'backward', 'optimizer', 'eval', 'sum']]
        self.aggregations += aggregations or []
        self.start_time = process_time()

    def log_time(self, name: str, time: float):
        self.writer.add_scalar(f'times.{name}', time)

    def update(self, step_num, val, val_train, loss,
               strategy, delay, state_dict,
               extras=None):

        self.writer.add_scalar('metrics.val', val)
        self.writer.add_scalar('metrics.val_train', val_train)
        self.writer.add_scalar('metrics.loss', loss)

        extras = extras or {}
        for key, v in extras.items():
            self.extras.add(key)
            self.writer.add_scalar(f'metrics.{key}', v)

        if mode_dct[self.mode] * val > mode_dct[self.mode] * self.best_val:
            self.best_at = step_num
            self.best_val = val
            self.writer.add('metrics_stats.best_val', val)
            self.writer.add('metrics_stats.best_val_at', step_num)
            self.writer.add_binary('strategy', strategy)
            if delay is not None:
                self.writer.add_binary('delay', delay)
            self.writer.add_binary('state_dict', state_dict)

    def dump(self):
        self.writer.add('times_stats.total', process_time() - self.start_time)
        self.aggregations += [(f'metrics.{key}', f'metrics_stats.{key}', 'arrayElemAt', self.best_at - 1) for key in
                              self.extras]
        self.writer.aggregate_stats(self.aggregations)

        data = self.writer.view()
        _, data = next(iter(data.items()))  # all writers should return the same data, taking the first one
        self.print_run(data)
        # returns either id for 'mongo' or data for 'disk'
        return self.writer.close()

    def print_run(self, data):
        metrics = data['metrics_stats']
        times = data['times_stats']
        msg = f"epoch {data['epoch']}:"
        if 'end_val' in metrics:
            msg += f"\tend val={metrics.get('end_val'):.2f}"
        if 'best_val' in metrics:
            msg += f"\tbest val={metrics.get('best_val'):.2f} at {metrics.get('best_val_at')}"
        # for key in self.extras:
        #     msg += f"\t{key}={metrics[key]:.1f}"
        msg += f"\ttime={times['total']:.2f}"
        msg += f"\tavg step time={times['avg_sum']:.3f}"
        logger.info(msg)
