import sys
from logging import DEBUG, INFO, WARNING, ERROR
import logging
from typing import List

import pymongo
from datetime import datetime

from typing_extensions import override

from src.utils import get_object


def sys_stdout():
    return sys.stdout


def add_root_handlers(handlers: dict) -> None:
    logger = logging.getLogger()
    for h in handlers.values():
        handler = h['handler']
        if 'level' in h:
            level = get_object(h['level'])
            handler.setLevel(level)
        if 'formatter' in h:
            handler.setFormatter(h['formatter'])
        logger.addHandler(handler)


def get_logger(name=None):
    # return logging.getLogger()
    logger = logging.getLogger('src')
    if name is None or name == '':
        return logger

    children = logger.getChild(name)
    # children.disabled = False
    return children


def set_logger_run_id(run_id: str) -> None:
    for handler in logging.getLogger().handlers:
        try:
            handler.set_run_id(run_id)  # noqa
        except AttributeError:
            pass


def set_logger_experiment_id(experiment_id: str) -> None:
    for handler in logging.getLogger().handlers:
        try:
            handler.set_experiment_id(experiment_id)  # noqa
        except AttributeError:
            pass


class MongoHandler(logging.Handler):
    """
    A logging handler that will record messages to a (optionally capped)
    MongoDB collection.
    ```
    connection = pymongo.Connection()
    collection = connection.db.log
    logger = logging.getLogger("mongotest")
    logger.addHandler(MongoHandler(drop=True))
    logger.error("Hello, world!")
    collection.find_one()['message']
    ```
    u'Hello, world!'
    """

    def __init__(self, device, level=logging.NOTSET, sync_after: int = 1):
        logging.Handler.__init__(self, level)

        self._collection = device.get_collection('logs')
        self._experiment_id = None
        self._run_id = None

        self.buffer = []
        self.sync_after = sync_after

    def set_run_id(self, run_id: str) -> None:
        self._run_id = run_id

    def set_experiment_id(self, experiment_id: str) -> None:
        self._experiment_id = experiment_id

    def _query(self, query):
        self.buffer.append(query)
        if len(self.buffer) >= self.sync_after:
            self._sync()

    def _sync(self):
        if self.buffer:
            self._collection.bulk_write(self.buffer)
            self.buffer[:] = []

    @override
    def emit(self, record):
        log = {
            'date_created': datetime.now(),
            'levelno': record.levelno,
            'levelname': record.levelname,
            'message': record.msg,
            'experiment_id': self._experiment_id,
            'run_id': self._run_id,
        }
        self._query(pymongo.InsertOne(log))

    @override
    def flush(self):
        self._sync()

    @override
    def close(self):
        self.flush()
