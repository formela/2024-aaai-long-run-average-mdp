# Standard Imports
from datetime import datetime
import os

# Third-party Imports
import bson
import numpy
import torch
from statistics import mean, pstdev
import pandas
import yaml

# Local Imports
from src.io.devices import DiskDevice
from src.postprocess.plotting import (
    plot_values,
    plot_times
)


agg_fn_dict = {
    'avg': mean,
    'stdDevPop': pstdev,
    'size': len,
    'max': max,
    'min': min,
    'sum': sum,
    'last': lambda x: x[-1],
    'arrayElemAt': lambda x, y: x[y],
}


def get_indexed_df(epoch, data_dict):
    df = pandas.DataFrame(data_dict)
    df['step'] = range(1, len(df) + 1)
    df['epoch'] = epoch
    return df.set_index(['epoch', 'step'])


class DiskWriter:
    def __init__(self, device: DiskDevice):
        self.data = {}
        self.add('date_created', datetime.now())
        self._id = None
        self.results_dir = device.path
        self.plots_dir = self.results_dir / 'plots'
        self.checkpoints_dir = self.results_dir / 'checkpoints'
        self.strategies_dir = self.results_dir / 'strategies'

    def parent(self, keys):
        curr = self.data
        for key in keys:
            curr = curr.setdefault(key, {})
        return curr

    def add(self, variable_name, value):
        keys = variable_name.split('.')
        keys, variable = keys[:-1], keys[-1]
        self.parent(keys)[variable] = value

    def get(self, variable_name):
        curr = self.data
        keys = variable_name.split('.')
        for key in keys:
            if key in curr:
                curr = curr[key]
            else:
                curr = None
                break
        return curr

    def add_binary(self, variable_name, value):
        self.add(variable_name, value)

    def view(self):
        return self.data

    def get_id(self):
        return self._id

    def close(self):
        self.add('date_finished', datetime.now())
        return self.data


class DiskExperimentWriter(DiskWriter):
    def __init__(self, device, plot: bool):
        super(DiskExperimentWriter, self).__init__(device)
        self.add('runs_df.metrics', pandas.DataFrame())
        self.add('runs_df.times', pandas.DataFrame())
        self.add('runs_df.metrics_stats', pandas.DataFrame())
        self.add('runs_df.times_stats', pandas.DataFrame())
        self.plot = plot
        if self.plot:
            os.makedirs(self.plots_dir, exist_ok=True)

    def add_run(self, run):
        for key in ['metrics', 'times']:
            # collect aggregations
            new = {'epoch': run['epoch'], **run[f'{key}_stats']}
            new_df = pandas.DataFrame([new]).set_index('epoch')
            current_df = self.get(f'runs_df.{key}_stats')
            self.add(f'runs_df.{key}_stats', pandas.concat([current_df, new_df]))

            # collect values
            new_df = get_indexed_df(run['epoch'], run[key])
            current_df = self.get(f'runs_df.{key}')
            self.add(f'runs_df.{key}', pandas.concat([current_df, new_df]))

    def aggregate_stats(self, aggregations):
        for var, new_var, agg_fn, *params in aggregations:
            key, var = var.split('.')
            value = agg_fn_dict[agg_fn](self.get('runs_df')[key][var], *params)
            self.add(new_var, value)

    def close(self):
        filename = os.path.join(self.results_dir, 'settings.yml')
        with open(filename, 'w') as stream:
            stream.write(yaml.dump(self.get('settings'), sort_keys=False))
        runs_df = self.get('runs_df')
        stats_df = pandas.concat([runs_df['metrics_stats'], runs_df['times_stats']], axis='columns')
        stats_df.sort_index(inplace=True)
        stats_df.to_csv(os.path.join(self.results_dir, 'stats_epoch.csv'))
        runs_df['metrics'].to_csv(os.path.join(self.results_dir, 'metrics_full.csv'))
        runs_df['times'].to_csv(os.path.join(self.results_dir, 'times_full.csv'))
        info = {key: val for key, val in self.data.items() if key not in ['runs_df', 'settings']}
        info_series = pandas.Series(info)
        info_series.to_csv(os.path.join(self.results_dir, 'stats_final.csv'), header=False)

        if self.plot:
            plots = {key: key for key in self.get('runs_df.metrics').columns}
            plot_values(self.get('runs_df.metrics').reset_index(),
                        os.path.join(self.plots_dir, 'training_progress.pdf'), plots=plots)
            plot_times(self.get('runs_df.times').reset_index(),
                       os.path.join(self.plots_dir, 'avg_times.pdf'))

        return super(DiskExperimentWriter, self).close()


class DiskRunWriter(DiskWriter):
    def __init__(self, device, epoch, save_checkpoints: bool, save_strategies: bool):
        super(DiskRunWriter, self).__init__(device)
        self._id = epoch
        self.save_checkpoints = save_checkpoints
        self.save_strategies = save_strategies
        if self.save_checkpoints:
            os.makedirs(self.checkpoints_dir, exist_ok=True)
        if self.save_strategies:
            os.makedirs(self.strategies_dir, exist_ok=True)

    def add_scalar(self, variable_name, value):
        keys = variable_name.split('.')
        keys, variable = keys[:-1], keys[-1]
        self.parent(keys).setdefault(variable, []).append(value)

    def aggregate_stats(self, aggregations):
        for var, new_var, agg_fn, *params in aggregations:
            value = agg_fn_dict[agg_fn](self.get(var), *params)
            self.add(new_var, value)

    def close(self):
        # get_indexed_df(self.id, self.data['plots']).to_csv(os.path.join(self.plots_dir, f'plots_{self.id:02d}.csv'))
        # get_indexed_df(self.id, self.data['times']).to_csv(os.path.join(self.plots_dir, f'times_{self.id:02d}.csv'))
        if self.save_checkpoints:
            torch.save(self.get('state_dict'),
                       os.path.join(self.checkpoints_dir, f'strategy_sd_{self._id:02d}.pth'))
        if self.save_strategies:
            numpy.save(os.path.join(self.strategies_dir, f'strategy_{self._id:02d}.npy'),
                       self.get('strategy'), allow_pickle=True)
            delay = self.get('delay')
            if delay is not None:
                numpy.save(os.path.join(self.strategies_dir, f'delay_{self._id:02d}.npy'),
                           delay, allow_pickle=True)

        return super(DiskRunWriter, self).close()
