# Standard Imports
from datetime import datetime

# Third-party Imports
from pymongo import UpdateOne
import bson
from bson.binary import Binary
import pickle

# Local Imports
from src.io.devices import MongoDevice
from src.io.logging import set_logger_run_id, set_logger_experiment_id


class MongoWriter:
    def __init__(self, device: MongoDevice, collection_name: str, sync_after: int) -> None:
        self._device = device
        self._collection = device.get_collection(collection_name)
        self._id = self._collection.insert_one({'date_created': datetime.now()}).inserted_id
        self._buffer = []
        self._sync_after = sync_after

    def _query(self, query):
        self._buffer.append(query)
        if len(self._buffer) >= self._sync_after:
            self._sync()

    def _sync(self):
        if self._buffer:
            self._collection.bulk_write(self._buffer)
            self._buffer[:] = []

    def add(self, variable_name, value):
        self._query(UpdateOne({'_id': self._id}, {'$set': {variable_name: value}}))

    def add_binary(self, variable_name, data):
        # https://stackoverflow.com/questions/6367589/saving-numpy-array-in-mongodb
        self.add(variable_name, Binary(pickle.dumps(data, protocol=2), subtype=128))

    def view(self):
        self._sync()
        return self._collection.find_one({"_id": self._id})

    def get_id(self):
        return self._id

    def close(self):
        self.add('date_finished', datetime.now())
        self._sync()
        return self._id


class MongoExperimentWriter(MongoWriter):
    def __init__(self, device: MongoDevice, sync_after: int):
        super().__init__(device, 'experiments', sync_after)
        set_logger_experiment_id(self._id)

        # argmax aggregation:
        # https://stackoverflow.com/questions/54292286/find-max-element-inside-an-array
        # https://stackoverflow.com/questions/65365695/how-to-get-argmax-argmin-of-multiple-fields-simultaneously-in-mongodb

    def add_run(self, run):
        self._query(UpdateOne({'_id': self._id}, {'$push': {'runs': run}}, upsert=True))

    def aggregate_stats(self, aggregations):
        self._sync()
        rules = {new_var: {f'${agg_fn}': [f'$runs_all.{var}', *param]} for var, new_var, agg_fn, *param in
                 aggregations}
        pipeline = [
            {'$match': {'_id': self._id}},
            {'$lookup': {
                'from': self._device.get_collection_name('runs'),
                'localField': 'runs',
                'foreignField': '_id',
                'as': 'runs_all'
            }},
            {'$set': rules},
            {'$project': {new_var: 1 for new_var in rules.keys()}},
            {'$merge': {
                'into': self._device.get_collection_name('experiments')},
             }
        ]
        self._collection.aggregate(pipeline)


class MongoRunWriter(MongoWriter):
    def __init__(self, device: MongoDevice, sync_after: int, epoch: int = None):
        super().__init__(device, 'runs', sync_after)
        set_logger_run_id(self._id)

    def add_scalar(self, variable_name, value):
        self._query(UpdateOne({'_id': self._id}, {'$push': {variable_name: value}}, upsert=True))

    def add_strategy(self, strategy, state_dict):
        self.add_binary('strategy', strategy)
        self.add_binary('state_dict', state_dict)

    def aggregate_stats(self, aggregations):
        rules = {f'{new_var}': {f'${agg_fn}': [f'${var}', *param]} for var, new_var, agg_fn, *param in aggregations}
        self._query(UpdateOne({'_id': self._id}, [{'$set': rules}]))
