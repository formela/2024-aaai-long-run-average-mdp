# Standard Imports
from typing import Any

# Local Imports
from src.io.writers.disk_witer import DiskExperimentWriter, DiskRunWriter
from src.io.writers.mongo_writer import MongoExperimentWriter, MongoRunWriter


class WritersDict:
    def __init__(self, writers: dict[str, Any]):
        self._writers = writers

    def __all_have_method_check(self, attr: str):
        if not all(hasattr(writer, attr) and callable(getattr(writer, attr))
                   for writer in self._writers.values()):
            raise AttributeError(f"Method '{attr}' not found in all writers.")

    def __getattr__(self, attr: str):
        if attr in self.__dict__:
            return self.__dict__[attr]

        self.__all_have_method_check(attr)

        def multi_writer_method(*args, **kwargs):
            results = {}
            for key, writer in self._writers.items():
                method = getattr(writer, attr)
                result = method(*args, **kwargs)
                results[key] = result
            return results

        return multi_writer_method

    def call_zipped(self, attr: str, *args, **kwargs):
        self.__all_have_method_check(attr)
        results = {}
        for key, writer in self._writers.items():
            method = getattr(writer, attr)
            keys_args = [arg[key] for arg in args]
            keys_kwargs = {k: v[key] for k, v in kwargs.items()}
            result = method(*keys_args, **keys_kwargs)
            results[key] = result
        return results
