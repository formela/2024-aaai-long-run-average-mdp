# Standard Imports
from abc import ABCMeta, ABC, abstractmethod
from pathlib import Path

# Third-party Imports
import pymongo
from typing_extensions import override


class NamedSingleton(type):
    _instances = {}

    def __call__(cls, name, *args, **kwargs):
        if (cls, name) not in cls._instances:
            print(f'Creating new instance of {cls.__name__} with name {name}')
            cls._instances[(cls, name)] = super(NamedSingleton, cls).__call__(name, *args, **kwargs)
        return cls._instances[(cls, name)]


class AbstractNamedSingleton(ABCMeta, NamedSingleton):
    pass


class IODevice(ABC):
    name: str

    def __init__(self, name: str) -> None:
        self.name = name

    @abstractmethod
    def close(self):
        raise NotImplementedError


class DiskDevice(IODevice):
    path: Path

    def __init__(self, name: str, path: str):
        super().__init__(name)
        self.path = Path(path)
        self.path.mkdir(parents=True, exist_ok=True)

    @override
    def close(self):
        pass

    def get_path(self) -> Path:
        return self.path


class MongoDevice(IODevice):
    def __init__(
            self, name:
            str, db_name: str,
            client: pymongo.MongoClient,
            collections: dict[str, str]
    ) -> None:
        super().__init__(name)
        self._client = client
        self._db_name = db_name
        self._db = self._client[db_name]
        self._collection_names = collections

    @override
    def close(self):
        self._client.close()

    @property
    def database(self) -> pymongo.database.Database:
        return self._db

    def get_collection_name(self, collection_key: str) -> str:
        collection_name = self._collection_names.get(collection_key)
        if collection_name is None:
            raise ValueError(f'Collection {collection_key} not found')
        return collection_name

    def get_collection(self, collection_key: str) -> pymongo.collection.Collection:
        collection_name = self.get_collection_name(collection_key)
        return self._db[collection_name]
