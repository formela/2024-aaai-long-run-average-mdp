import importlib
import random
import numpy
import torch


def set_seed(seed: int, incr: int) -> int:
    seed += incr
    random.seed(seed)
    torch.manual_seed(seed)
    # torch.cuda.manual_seed(seed)
    numpy.random.seed(seed)
    return seed


def get_object(full_path):
    """Retrieves a class given fully qualified path for an object - including libraries."""
    if full_path is None:
        return None
    module_name, object_name = full_path.rsplit('.', 1)
    module = importlib.import_module(module_name)
    return getattr(module, object_name)
