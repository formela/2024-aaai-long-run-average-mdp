import argparse
import glob
import os

import pandas
from plotting import plot_values

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Plot training values')
    parser.add_argument(dest='files', type=str, help='Input csv file(s)')
    parser.add_argument('--filename', dest='output', type=str, help='Output pdf file name', default='training_progress')
    args = parser.parse_args()

    matching_files = glob.glob(args.files, recursive=True)
    if not matching_files:
        print(f"No files matching {args.files}")
        exit(2)

    for filename in matching_files:
        if filename.endswith('.csv'):
            try:
                log_data = pandas.read_csv(filename)
                out_name = os.path.join(os.path.dirname(filename), args.output + '.pdf')
                plot_values(log_data, out_name)
            except Exception as e:
                print(f"{filename} parsing failed with error: {e}, skipping")
        else:
            print(f'Skipping {filename}: not a csv file')
