# Standard Imports
from typing import Union, List, Optional, Tuple, Dict, Any
from collections import Counter

# Third-party imports
import torch
import networkx
import numpy as np
from termcolor import colored

# Local imports
from src.graph.collections import graphs_dict
from src.graph.modifiers import graph_modifier_from_string
import src.io.logging as logging
from src.io.logging import get_logger


logger = get_logger('graph')


def get_node_map(nodes):
    return {node: index for index, node in enumerate(nodes)}


class Graph:
    def __init__(self,
                 topology,
                 goals: Optional[List] = None,
                 name=None,
                 device='cpu',
                 data_type=torch.float64):
        self.name = name
        # todo: move to trainer
        self.device = device
        self.data_type = data_type

        self.graph = topology
        self.graph = self.graph.to_directed()

        self.nodes, self.nodes_n, self.targets, self.targets_n, self.non_targets = self.prepare_nodes()
        self.target_features, self.non_target_features = self.collect_features()

        # integer encoding for external evaluators
        self.node_map = get_node_map(self.nodes)
        self.times, self.mask = self._graph2torch(self.graph, self.nodes)
        self.memory = self.target_features['memory'] + self.non_target_features['memory']

        if goals is None:
            self.goal_sets = []
            self.costs = []
        else:
            self.goal_sets, self.costs = zip(*[[g['nodes'], g.get('value', 0)] for g in goals])

        self.goal_sets = list(self.goal_sets)
        self.costs = list(self.costs)

        self.goal_sets += [
            [t] for t in self.targets
        ]
        self.goal_sets = [[self.node_map[t] for t in g] for g in self.goal_sets]
        self.costs += list(self.target_features['value'])
        self.costs = torch.tensor(self.costs, dtype=self.data_type, device=self.device, requires_grad=False)

        rewards = self.target_features['rewards']
        delimiters = self.target_features['delimiters']
        nan_rewards = [type(x) is float and np.isnan(x) for x in rewards]
        if not (all(nan_rewards)):
            # there are some rewards
            for rew, deli in zip(rewards, delimiters):
                if isinstance(rew, list):
                    rew += [0] * (2 + len(deli) - len(rew))
            self.reward_decay = torch.tensor(sum([reward[-1] for reward in self.target_features['rewards']]))

        self.print_graph_stats(level=logging.DEBUG)

    def _graph2torch(self, graph, nodes):
        # edge_len matrix indexed by nodes of the graph
        times = networkx.to_numpy_array(graph, nodelist=nodes, weight='len')
        times = torch.tensor(times, dtype=self.data_type, device=self.device, requires_grad=False)

        # binary matrix; edge encodings of the graph
        mask = times > 0

        return times, mask

    def prepare_nodes(self):
        targets, non_targets = [], []
        for (node, is_target) in sorted(self.graph.nodes.data('target')):
            (non_targets, targets)[is_target].append(node)
        targets_n = len(targets)

        nodes = targets + non_targets  # external evaluators require targets first
        nodes_n = len(nodes)

        return nodes, nodes_n, targets, targets_n, non_targets

    def collect_features(self):
        target_features = ['value', 'memory', 'attack_len', 'blindness', 'delimiters', 'rewards']
        target_features = get_node_features(self.graph, self.targets, target_features)
        non_target_features = ['memory']
        non_target_features = get_node_features(self.graph, self.non_targets, non_target_features)
        return target_features, non_target_features

    def get_patrolling_init_data(self):
        data = {'memory': self.target_features['memory'] + self.non_target_features['memory'],
                'attack_len': self.target_features['attack_len'],
                'value': self.target_features['value'],
                'blindness': self.target_features['blindness'],
                'edges': self.edges}
        return data

    def get_maintenance_init_data(self):
        data = {'memory': self.target_features['memory'] + self.non_target_features['memory'],
                'delimiters': self.target_features['delimiters'],
                'rewards': self.target_features['rewards'],
                'edges': self.edges}
        return data

    def __repr__(self):
        return self.name or self.graph.name or self.__class__.__name__

    def print_graph_stats(self, level=logging.INFO):
        # TODO: test
        if level < logger.level:
            return
        logger.log(level, colored(f'Nodes: ({self.graph.number_of_nodes()})', 'blue'))
        for node in self.graph.nodes.data():
            logger.log(level, f'  {node}')
        logger.log(level, colored(f'Edges: ({self.graph.number_of_edges()})', 'blue'))
        for edge in self.graph.edges.data():
            logger.log(level, f'  {edge}')
        logger.log(level, colored(f'Incidence matrix', 'blue'))
        with np.printoptions(precision=1, suppress=True, threshold=np.inf, linewidth=np.inf):
            logger.log(
                level, f"\n{np.array2string(networkx.to_numpy_array(self.graph, nodelist=self.nodes, weight='len'))}")
        lengths = [l for _, _, l in self.graph.edges.data('len')]
        logger.log(level, colored('Graph statistics:', 'blue'))
        logger.log(level, f'  edge length: min={min(lengths)}  max={max(lengths)}  avg={np.mean(lengths):.2f}')
        logger.log(level, f'  degrees:')
        if self.graph.is_directed():
            out_deg = Counter(d for n, d in self.graph.out_degree())
            in_deg = Counter(d for n, d in self.graph.in_degree())
            for (out_degree, out_count), (in_degree, in_count) in zip(out_deg.most_common(), in_deg.most_common()):
                logger.log(level, f'    in={in_degree:01d}:  {in_count}x  out={out_degree:01d}:  {out_count}x')
        logger.log(level, colored('Target features', 'blue'))
        for key, features in self.target_features.items():
            logger.log(level, colored(f'  {key}:', 'blue'))
            logger.log(level, f'  {features}')

    def get_figure(self):
        # TODO: test
        if self.graph.name.__contains__('square_subgraph'):
            pos = {node: node for node in self.graph.nodes()}
        # elif self.graph.name.__contains__('office_building'):
        #     pos = networkx.planar_layout(self.graph)
        else:
            pos = networkx.spring_layout(self.graph)
        networkx.draw(self.graph, with_labels=True, pos=pos)
        labels = {tuple(edge): label for *edge, label in self.graph.edges.data('len')}
        networkx.draw_networkx_edge_labels(self.graph, pos=pos, edge_labels=labels, label_pos=0.3)
        # plt.savefig(os.path.join(self.results_dir, 'graph.pdf'))
        # todo: return fig binary
        # https://stackoverflow.com/questions/31492525/converting-matplotlib-png-to-base64-for-viewing-in-html-template
        # https://stackoverflow.com/questions/57222519/how-to-get-a-figure-binary-object-by-matplotlib
        return 0


def get_graph(*args, **kwargs):
    return Graph(*args, **kwargs)


def graph_loader_from_string(loader):
    dct = {'identity': lambda graph: graph,
           **graphs_dict}
    return dct[loader]


def load_graph_with_modifiers(loader, loader_params, modifiers, level=logging.INFO):
    graph = graph_loader_from_string(loader)(**loader_params)
    modifiers = modifiers or {}
    logger.log(level, colored('Graph preprocessing:', 'green'))
    for modifier, modifier_params in modifiers.items():
        logger.log(level, colored(f' {modifier}', 'green'))
        graph = graph_modifier_from_string(modifier)(graph, **modifier_params)
    logger.log(level, colored('Graph preprocessing done', 'green'))
    return graph


def get_node_features(graph, nodes, features):
    return {key: [graph.nodes[n].get(key, float('nan')) for n in nodes] for key in features}
