from collections import defaultdict
import random
import networkx

from src.io.logging import get_logger
logger = get_logger('graph.modifiers')


def floyd_warshall(graph, visiting_bonus):
    dist = defaultdict(lambda: defaultdict(lambda: float("inf")))
    for s, t, length in graph.edges.data('len'):
        dist[s][t] = min(length - visiting_bonus, dist[s][t])

    for w in graph:
        dist_w = dist[w]
        for u in graph:
            dist_u = dist[u]
            for v in graph:
                d = dist_u[w] + dist_w[v]
                if d < dist_u[v]:
                    dist_u[v] = d

    edges_to_remove = []
    for s, t, length in graph.edges.data('len'):
        if dist[s][t] < length - visiting_bonus:
            edges_to_remove.append((s, t))
            logger.debug(f'  Removing edge: {s}--{t}')
    graph.remove_edges_from(edges_to_remove)
    return graph


def delete_k_rnd_edges(graph, k, seed=None):
    """Generates and returns a random graph as a modification of 'source_graph'.
       'k' randomly chosen edges are deleted such that the final graph is still strongly connected.
        @param verbose:
        @param graph:          the original graph
        @param k:              the number of edges to be deleted
        @param seed:           the seed for pseudorandom functions
    """
    graph = networkx.DiGraph(graph)
    edge_list = list(graph.edges(data=True))

    if seed is not None:
        random.seed(seed)

    while k > 0:
        chosen_edge = random.choice(edge_list)
        graph.remove_edge(chosen_edge[0], chosen_edge[1])
        if networkx.is_strongly_connected(graph):
            k -= 1
            edge_list.remove(chosen_edge)
            logger.debug(f'  Removing edge: {chosen_edge[0]}--{chosen_edge[1]}')
        else:
            graph.add_edges_from([chosen_edge])
    return graph


def shake_values(graph, prob, shift, seed=None):
    if seed is not None:
        random.seed(seed)

    for node, att in graph.nodes(data=True):
        if att['target'] and random.uniform(0, 1) < prob:
            sign = random.choice([-1, 1])
            att['value'] *= (1.0 + sign*shift)
            att['value'] = max(0, att['value'])
            logger.debug(f'  Changing value of {node}')

    return graph


def shake_edge_lengths(graph, prob, shift, seed=None):
    if seed is not None:
        random.seed(seed)

    for s, t, att in graph.edges(data=True):
        if random.uniform(0, 1) < prob:
            sign = random.choice([-1, 1])
            att['len'] *= (1.0 + sign*shift)
            att['len'] = max(0, att['len'])
            logger.debug(f'  Changing length of {s}--{t}')

    return graph


def make_oriented(graph):
    return graph.to_directed()


def truncated_rands(sigma):
    while True:
        res = random.gauss(0, sigma)
        if -4 * sigma < res < 4 * sigma:
            return res


def set_rnd_values(graph, min_val, max_val, seed=None):
    if seed is not None:
        random.seed(seed)

    for node in graph.nodes(data=True):
        graph.nodes[node]['value'] = random.uniform(min_val, max_val)

    return graph


def graph_modifier_from_string(modifier):
    dct = {'floyd_warshall': floyd_warshall,
           'shake_values': shake_values,
           'shake_edge_lengths': shake_edge_lengths,
           'delete_k_rnd_edges': delete_k_rnd_edges,
           'make_oriented': make_oriented}
    return dct[modifier]
