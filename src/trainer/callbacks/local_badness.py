# Standard Imports
import functools
from time import process_time

# Third-party Imports
import torch


# Local Imports
from src.cpp_extensions.evaluators import StabilityLocalBadness
from src.trainer import StrategyTrainer as Trainer
from src.trainer.callbacks import Callback
from src.io.logging import get_logger

logger = get_logger('callback/noise')


# not used
def get_badness_aggregation(at: int, agg_fnc: str) -> tuple[str, str, str]:
    return f'metrics_stats.l-badness_at_{at}', f'metrics.l-badness_at_{at}_{agg_fnc}', agg_fnc


class LocalBadnessCallback(Callback):
    evaluator: StabilityLocalBadness

    def __init__(self, evaluator: functools.partial['StabilityLocalBadness']):
        self.partial_evaluator = evaluator
        self.best_val = float('inf')
        self.best_strategy = None

    def on_validation_step_end(self, trainer: Trainer) -> None:
        if trainer.val < self.best_val:
            self.best_val = trainer.val
            self.best_strategy = trainer.strategy

    def on_train_start(self, trainer: Trainer) -> None:
        objective = trainer.model.objective
        self.evaluator = self.partial_evaluator(target_to_goal=objective.target_to_goal, aug_nodes=objective.aug_nodes)

    def on_train_end(self, trainer: Trainer) -> None:
        self._report_local_badnesses(trainer)

    def _report_local_badnesses(self, trainer: Trainer) -> None:
        if self.best_strategy is None:
            return
        strategy = torch.from_numpy(self.best_strategy)
        # get_node_stationary accepts BSCC x Augnodes x Augnodes and returns BSCC x Augnodes
        inv = trainer.model.objective.get_node_stationary(strategy[None, :])[0]

        start_time = process_time()
        local_badnesses = self.evaluator.forward(strategy, inv)
        trainer.report.writer.add('times_stats.badness_dp', process_time() - start_time)

        for name, val in local_badnesses.items():
            trainer.report.writer.add(f'metrics_stats.{name}', val)

        # start_time = process_time()
        # local_badnesses = self.evaluator.forward(strategy, inv, use_dfs=True)
        # trainer.report.writer.add('times_stats.badness_dfs', process_time() - start_time)
