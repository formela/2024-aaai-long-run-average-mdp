# Third-party Imports
from termcolor import colored
import numpy as np

# Local Imports
from src.trainer import StrategyTrainer as Trainer
from src.trainer.callbacks import Callback
from src.io.logging import get_logger

logger = get_logger('callback/printing')


class PrintCallback(Callback):
    def on_train_step_start(self, trainer: Trainer) -> None:
        logger.debug(colored(f'step={trainer.step_num} (train) started', 'green'))

    def on_validation_step_end(self, trainer: Trainer) -> None:
        logger.debug(colored(f'step={trainer.step_num} (eval)\t val={trainer.val:.2f}', 'cyan'))
        # TODO: print strategy
        # self.model.graph.print_strategy(
        #     self.strategy,
        #     # delay=self.model.delay,
        #     style=self.strategy_style,
        #     precision=3,
        #     level=logging.DEBUG,
        # )

    def on_before_backward(self, trainer: Trainer) -> None:
        logger.debug(
            colored(f'step={trainer.step_num} (train)\t val={trainer.val_train:.2f}\t loss={trainer.loss:.2f}',
                    'green'))
        if trainer.strategy is not None:
            with np.printoptions(precision=3, suppress=True, threshold=np.inf, linewidth=np.inf):
                logger.debug(
                    colored(np.array2string(trainer.strategy, prefix='  '), 'blue'))

    def on_after_backward(self, trainer: Trainer) -> None:
        with np.printoptions(precision=3, suppress=True, threshold=np.inf, linewidth=np.inf):
            logger.debug(
                colored(f'Strategy grad:\n', 'blue') + np.array2string(trainer.model.strategy.params.grad.numpy(),
                                                                       prefix='  '))
            # TODO
            # if self.model.delay is not None:
            #     logger.debug(colored(f'Delay grad:\n', 'blue') + np.array2string(self.model.delay.params.grad.numpy(), prefix='  '))
