# Standard Imports
from abc import abstractmethod, ABC
import math

# Third-party Imports
import torch

# Local Imports
from src.trainer import StrategyTrainer as Trainer
from src.trainer.callbacks import Callback
from src.io.logging import get_logger

logger = get_logger('callback/noise')


class AbstractDecay(ABC):
    @abstractmethod
    def apply(self, step: int) -> float:
        ...


class ExpDecay(AbstractDecay):
    def __init__(self, tau=1):
        self.tau = tau

    def apply(self, step):
        return math.exp(-self.tau * step)


class PolynomialDecay(AbstractDecay):
    def __init__(self, ord):
        self.ord = ord

    def apply(self, step):
        return 1.0 / (step ** self.ord)


class AbstractNoise(ABC):
    @abstractmethod
    def apply(self, val: float, step: int) -> float:
        ...


class ConstNoise(AbstractNoise):
    def __init__(self, amplitude):
        self.amplitude = amplitude

    def apply(self, val, step):
        return self.amplitude


class BasicNoise(AbstractNoise):
    def __init__(self, amplitude: int, decay: AbstractDecay):
        self.amplitude = amplitude
        self.decay = decay

    def apply(self, val, step):
        return self.amplitude * self.decay.apply(step)


class IncreaseNoiseOnPlateau(AbstractNoise):
    def __init__(self, amplitude, threshold, averaging, cooldown, decay: AbstractDecay):
        self.amplitude = amplitude
        self.decay = decay
        self.averaging = averaging
        self.threshold = threshold
        self.cooldown = cooldown
        self.vals = []
        self.cooldown_counter = 0

    def apply(self, val, step):
        self.vals.append(val)
        al = self.averaging

        if step >= 2 * al:
            curr_sum = sum(self.vals[-al:])
            prev_sum = sum(self.vals[-2 * al:-al])
            if 1 < curr_sum / prev_sum < 1 + self.threshold and self.cooldown_counter >= 0:
                self.amplitude += 1.0
                self.cooldown_counter = -self.cooldown
            else:
                self.cooldown_counter += 1

        return self.amplitude * self.decay.apply(step)


class GradientNoiseCallback(Callback):

    def __init__(self, noise: AbstractNoise):
        self.noise = noise

    def on_before_optimizer_step(self, trainer: Trainer) -> None:
        rand_noise = torch.clamp(torch.randn_like(trainer.model.strategy.params.grad), -3.0, 3.0)
        step_noise = rand_noise * self.noise.apply(trainer.val_train, trainer.step_num)
        trainer.model.strategy.params.grad += step_noise
