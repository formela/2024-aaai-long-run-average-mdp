# Local Imports
from src.trainer import StrategyTrainer as Trainer


class Callback:
    """
    Base class used to build new callbacks.
    Subclass this class and override any of the relevant hooks.
    Inspired by `lightning.pytorch.callbacks.Callback`.
    """

    def on_train_start(self, trainer: Trainer) -> None:
        ...

    def on_train_end(self, trainer: Trainer) -> None:
        ...

    def on_validation_start(self, trainer: Trainer) -> None:
        ...

    def on_validation_end(self, trainer: Trainer) -> None:
        ...

    def on_train_step_start(self, trainer: Trainer) -> None:
        ...

    def on_train_step_end(self, trainer: Trainer) -> None:
        ...

    def on_validation_step_start(self, trainer: Trainer) -> None:
        ...

    def on_validation_step_end(self, trainer: Trainer) -> None:
        ...

    def on_before_backward(self, trainer: Trainer) -> None:
        ...

    def on_after_backward(self, trainer: Trainer) -> None:
        ...

    def on_before_optimizer_step(self, trainer: Trainer) -> None:
        ...

    def on_before_zero_grad(self, trainer: Trainer) -> None:
        ...
