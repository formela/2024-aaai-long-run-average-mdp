# Local Imports
from src.trainer import StrategyTrainer as Trainer
from src.trainer.callbacks import Callback


class TestCallback(Callback):
    def on_train_start(self, trainer: Trainer) -> None:
        print('on_train_start')

    def on_train_end(self, trainer: Trainer) -> None:
        print('on_train_end')

    def on_validation_start(self, trainer: Trainer) -> None:
        print('on_validation_start')

    def on_validation_end(self, trainer: Trainer) -> None:
        print('on_validation_end')

    def on_train_step_start(self, trainer: Trainer) -> None:
        print('on_train_step_start')

    def on_train_step_end(self, trainer: Trainer) -> None:
        print('on_train_step_end')

    def on_validation_step_start(self, trainer: Trainer) -> None:
        print('on_validation_step_start')

    def on_validation_step_end(self, trainer: Trainer) -> None:
        print('on_validation_step_end')

    def on_before_backward(self, trainer: Trainer) -> None:
        print('on_before_backward')

    def on_after_backward(self, trainer: Trainer) -> None:
        print('on_after_backward')

    def on_before_optimizer_step(self, trainer: Trainer) -> None:
        print('on_before_optimizer_step')

    def on_before_zero_grad(self, trainer: Trainer) -> None:
        print('on_before_zero_grad')
