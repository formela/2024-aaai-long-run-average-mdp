# Standard Imports
import functools
from typing import Optional
from copy import deepcopy
from time import process_time

# Third-party Imports
import torch

# Local Imports
from src.io.reporting import RunReporting
from src.io.logging import get_logger
from src.model.models.base_matrix_model import MatrixModel


logger = get_logger('trainer')


class StrategyTrainer(object):
    optimizer: torch.optim.Optimizer
    scheduler: torch.optim.lr_scheduler.LRScheduler

    def __init__(self,
                 model: MatrixModel,
                 report: RunReporting,
                 steps: int = 0,
                 eval_every: int = 1,
                 optimizer: Optional[functools.partial] = None,
                 scheduler: Optional[functools.partial] = None,
                 callbacks: Optional[dict] = None
                 ) -> None:
        if torch.cuda.is_available():
            model.cuda()
        self.model = model
        self.report = report
        self.steps = steps
        self.eval_every = eval_every
        self.optimizer = optimizer  # type: ignore
        self.scheduler = scheduler  # type: ignore
        self._configure_optimizers()
        self.callbacks = callbacks or {}

        self.step_num = 0
        self.val = 0.0
        self.strategy = None
        self.delay = None
        self.state_dict = None
        self.val_train = None
        self.loss = None
        self.times = None
        self.extras = None
        self.eval_extras = {}

    def eval(self):
        validation_start_time = process_time()
        self.model.eval()
        with torch.no_grad():
            self.val, self.strategy, self.eval_extras = self.model()
            self.state_dict = deepcopy(self.model.state_dict())
            # TODO: fix this
            # if self.model.delay is not None:
            #     self.delay = self.model.delay().numpy()
        self.report.log_time('eval', process_time() - validation_start_time)

    def step(self):
        start_time = process_time()
        self.model.train()
        self._callbacks('on_before_zero_grad')
        self.optimizer.zero_grad()
        self.model.schedule(self.steps, self.step_num)  # todo: move to callbacks?
        self.loss, self.val_train, self.extras = self.model()
        forward_time = process_time()
        self.report.log_time('forward', forward_time - start_time)

        self._callbacks('on_before_backward')
        self.loss.backward()
        self._callbacks('on_after_backward')
        backward_time = process_time()
        self.report.log_time('backward', backward_time - forward_time)

        self._callbacks('on_before_optimizer_step')
        self.optimizer.step()
        if self.scheduler is not None:
            self.scheduler.step()
        optimizer_time = process_time()
        self.report.log_time('optimizer', optimizer_time - backward_time)
        self.report.log_time('sum', optimizer_time - start_time)

    def train(self):
        self._callbacks('on_train_start')

        while self.step_num < self.steps:
            self.step_num += 1
            self._callbacks('on_train_step_start')
            self.step()
            self._callbacks('on_train_step_end')

            if self.step_num % self.eval_every == 0 or self.step_num == 1:
                self._callbacks('on_validation_step_start')
                self.eval()
                self._callbacks('on_validation_step_end')
            else:
                self.report.log_time('eval', 0)

            self.log()
            # todo: create early stopping callback
            if self.model.objective.early_stop(self.val):
                break

        self._callbacks('on_train_end')

    def validate(self):
        self._callbacks('on_validation_start')
        self.eval()
        self.log()
        self._callbacks('on_validation_end')

    def log(self):
        self.extras = self.extras or {}
        self.eval_extras = self.eval_extras or {}
        self.report.update(
            step_num=self.step_num,
            val=self.val,
            val_train=self.val_train,
            loss=self.loss.item() if self.loss else self.loss,
            strategy=self.strategy,
            delay=self.delay,
            state_dict=self.state_dict,
            extras={**self.extras, **self.eval_extras},
        )

    def _configure_optimizers(self) -> None:
        if self.optimizer is not None:
            params = []
            for child in self.model.children():
                if list(child.parameters()):
                    param_dict = {'params': child.parameters()}
                    if hasattr(child, 'lr') and child.lr is not None:
                        param_dict['lr'] = child.lr
                    params.append(param_dict)
            self.optimizer = self.optimizer(params=params)  # noqa
        else:
            raise AttributeError('Optimizer not set')
        if self.scheduler is not None:
            self.scheduler = self.scheduler(self.optimizer)  # noqa

    def _callbacks(self, phase: str):
        for name, callback in self.callbacks.items():
            callback.__getattribute__(phase)(self)
