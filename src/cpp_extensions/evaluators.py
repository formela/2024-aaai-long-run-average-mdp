# Standard Imports
from typing import Any, Dict, List, Optional, Tuple

# Third-party Imports
import hydra.utils
import torch

# # Uncomment for JIT compilation
# from torch.utils.cpp_extension import load
#
# cpp_evaluators = load(name="cpp_evaluators",
#                  sources=["src/main.cpp"],
#                  verbose=True)

# Local Imports
from cpp_evaluators import CppStabilityEvaluator


def create_distribution_from_ratios(goal_ratios: list[float]) -> torch.Tensor:
    goal_ratios_tensor = torch.as_tensor(goal_ratios, dtype=torch.float64)
    goal_distribution = goal_ratios_tensor / goal_ratios_tensor.sum(dim=-1, keepdim=True)
    return goal_distribution


class StabilityLocalBadness(torch.nn.Module):
    def __init__(self,
                 target_to_goal: Dict[int, int],
                 aug_nodes: List[Tuple[Tuple[Tuple[int, int], ...], int]],
                 goal_ratios: List[float],
                 d: int,
                 metric: str,
                 weights: Optional[List[int]] = None
                 ) -> None:
        super().__init__()

        if not weights:
            weights = [1]*(len(set(target_to_goal.values())))

        wanted_distr = create_distribution_from_ratios(goal_ratios).tolist()
        metric = hydra.utils.get_object(metric)
        states = self._create_states(target_to_goal, aug_nodes)
        dp_computation = CppStabilityEvaluator.ComputationTag.DP

        # dp_computation = CppStabilityEvaluator.ComputationTag.DP

        self._evaluator_dp = CppStabilityEvaluator(q2t=states,
                                                   weights=weights,
                                                   wanted_distr=wanted_distr,
                                                   computation=dp_computation,
                                                   metric=metric)
        self._evaluator_dfs = CppStabilityEvaluator(q2t=states,
                                                    weights=weights,
                                                    wanted_distr=wanted_distr,
                                                    computation=CppStabilityEvaluator.ComputationTag.DFS,
                                                    metric=metric)

        self._window_lengths_list = list(range(1, d + 1))

    @staticmethod
    def _create_states(
        target_to_goal: Dict[int, int],
        aug_nodes: List[Tuple[Tuple[Tuple[int, int], ...], int]]
    ) -> list[int]:

        states = [target_to_goal.get(x[0][0][0], -1) for x in aug_nodes]
        return states

    def forward(self, strategy: torch.Tensor, stationary_distr: torch.Tensor, use_dfs: bool = False) -> dict[str, float]:
        # shape: (d, n)
        list_of_penalties = self.get_list_of_penalties(strategy, self._evaluator_dfs if use_dfs else self._evaluator_dp)
        # shape: (d)
        badnesses = (list_of_penalties * stationary_distr[None, ...]).sum(dim=-1, keepdim=False)
        minimum = float(badnesses[0])

        monotone_badnesses: List[float] = []

        for badness in badnesses:
            minimum = min(float(badness), minimum)
            monotone_badnesses.append(minimum)

        result = {f'l-badness_at_{i}': monotone_badnesses[i - 1] for i in self._window_lengths_list}
        return result

    def get_list_of_penalties(self, strategy: torch.Tensor, evaluator: Any) -> torch.Tensor:
        list_of_penalties: torch.Tensor = evaluator.eval(strategy=strategy,
                                                         window_lengths_list=self._window_lengths_list)
        return list_of_penalties


class EvaluatorWrapper(torch.autograd.Function):
    @staticmethod
    def forward(ctx, strategy, evaluator, evaluator_params):
        prob = evaluator.forward(strategy.detach(), **evaluator_params)
        ctx.evaluator = evaluator
        return prob

    @staticmethod
    def backward(ctx, grad):
        strategy_grad = ctx.evaluator.backward(grad)
        return strategy_grad, None, None
