#include "utils.h"

namespace STABILITY
{

struct ADJ
//information about an adjacent vertex in a predecessor/successor list
{
	int To;
};

struct STRATEGY
//structure describing a strategy (note that some edges
//of the graph may be unused by the strategy)
{
	ARRAY2D<double, MAXQ, MAXQ> Prob; //Prob[i][j] is the probability of going to state j when being at state i
	ARRAY1D<int, MAXQ> OutDegree; //OutDegree[i] is the number of actual successors of state i
	ARRAY2D<ADJ, MAXQ, MAXQ> Succ; //Succ[i][j] is the j-th actual successor of state i
	ARRAY1D<int, MAXQ> InDegree; //InDegree[i] is the number of actual predecessors of state i
	ARRAY2D<ADJ, MAXQ, MAXQ> Pred; //Pred[i][j] is the j-th actual predecessor of state i

	ARRAY1D<int, MAXQ> IsReachable; //whether each state is reachable
};


struct GRAPH
{
	struct PAR_MANAGER_CALLER
	{
		PAR_MANAGER_CALLER(GRAPH* G,
		                   const std::vector<int>& q2t)
		{
			ParManager.Init(G, sizeof(GRAPH), q2t);
		}
	}
	PMC;

	int q; //number of states
	int g; //number of targets
	ARRAY1D<int, MAXQ> Q2T; //Q2V[i] is the target to which state i corresponds (-1 for no target)

	ARRAY1D<int, MAXQ> Stamp; //the time stamp for cycle search
	ARRAY1D<int, MAXQ> Q; //queue for reachability BFS
	int QBeg, QEnd;

	void InitStrategyAdj();
	void FindReachableStates();

	enum METRIC_TAG {L1, L2, L2Squared, FIRST_HARD, HardMax = FIRST_HARD, HardSum};
	enum COMPUTATION_TAG {DFS, DP};

	const METRIC_TAG MetricUsed;
	const bool UseVertexSetting;
	const COMPUTATION_TAG ComputationType;

	GRAPH(const std::vector<int>& q2t /*[Q] (init)*/,
          METRIC_TAG metricUsed, const std::vector<double>& distr /*[G] or [2 * G]*/,
          const std::vector<double>& weight /*[G]*/,
          bool vSetting, COMPUTATION_TAG computationType):
              PMC(this, q2t), MetricUsed(metricUsed),
              UseVertexSetting(vSetting), ComputationType(computationType)
	{
		ParManager.InitFinished();
		this->Init(q2t, distr, weight);
	}

	void Init(const std::vector<int>& q2t /*[Q]*/,
	          const std::vector<double>& distr /*[G] or [2 * G]*/,
	          const std::vector<double>& weight /*[G]*/);

	void LoadStrategy(at::Tensor /*double[Q][Q]*/ strat, bool CareForReachability = 1);

	at::Tensor /*double[Q]*/ PyEvaluateStrategy(at::Tensor /*double[Q][Q]*/ strat,
	                                            const std::vector<int>& dList);

	STRATEGY S;
	ARRAY1D<double, MAXG> Weight;
	ARRAY1D<double, MAXG> WantedFreq;
	ARRAY1D<double, MAXG> WantedLowerFreq;
	ARRAY1D<double, MAXG> WantedUpperFreq;
	ARRAY1D<double, MAXG> CurFreq;
	ARRAY1D<std::vector<double>, MAXQ> DistrPenalty;
	double* CurDistrPenalty;
	const std::vector<int>* p_dList;
	int dListSize;

	void Check_dList();
	void ComputeValue();
	void Search_DFS(int i, double p, int dCur, const std::vector<int>& FreqVector, int di);
	void Search_DP(int InitialState);

	double GetCurrentPenalty();
	static double PenaltyFuncL1(double a, double b);
	static double PenaltyFuncL2Squared(double a, double b);
};


void GRAPH::LoadStrategy(at::Tensor /*double[Q][Q]*/ strat, bool CareForReachability /*= 1*/)
{
	if(strat.dim() != 2)
	{
		THROW("LoadStrategy: q*q array expected, got " << strat.dim() << " dimension(s)!");
	}

	if((strat.size(0) != q) || (strat.size(1) != q))
	{
		THROW("LoadStrategy: q*q array expected for q = " << q << ", got " \
		      << strat.size(0) << "*" << strat.size(1) << "!");
	}

	double* p = strat.data_ptr<double>();
	for(int i = 0; i < q; i++)
	{
		for(int j = 0; j < q; j++)
		{
			S.Prob[i][j] = *(p++);
		}
	}

	InitStrategyAdj();

	if(CareForReachability)
	{
		FindReachableStates();
	}
	else
	{
		for(int i = 0; i < q; i++)
		{
			S.IsReachable[i] = 1;
		}
	}
}

at::Tensor /*double[Q]*/ GRAPH::PyEvaluateStrategy(at::Tensor /*double[Q][Q]*/ strat,
	                                               const std::vector<int>& dList)
{
	LoadStrategy(strat);

	p_dList = &dList;
	dListSize = dList.size();
	Check_dList();

	ComputeValue();

	at::Tensor rsl = torch::empty({dListSize, q}, torch::kFloat64);
	double* p = rsl.data_ptr<double>();
	for(int i = 0; i < dListSize; i++)
	{
		for(int j = 0; j < q; j++)
		{
			*(p++) = (S.IsReachable[j]) ? DistrPenalty[j][i] : -1.;
		}
	}

	return rsl;
}

void GRAPH::Init(const std::vector<int>& q2t /*[Q]*/,
                 const std::vector<double>& distr /*[G] or [2 * G]*/,
                 const std::vector<double>& weight /*[G]*/)
{
	q = ParManager.q;
	g = ParManager.g;

	for(int i = 0; i < q; i++)
	{
		Q2T[i] = q2t[i];
	}


	if(MetricUsed >= FIRST_HARD)
	{
		if((int) distr.size() != 2 * g)
		{
			THROW("LoadDistribution: for hard metric, expected array of length 2*g for g = "
			  << g << ", got length " << distr.size() << "!");
		}

		for(int i = 0; i < g; i++)
		{
			WantedLowerFreq[i] = distr[i];
			WantedUpperFreq[i] = distr[i + g];
		}
	}
	else
	{
		if((int) distr.size() != g)
		{
			THROW("LoadDistribution: for soft metric, expected array of length g for g = "
			  << g << ", got " << distr.size() << "!");
		}

		for(int i = 0; i < g; i++)
		{
			WantedFreq[i] = distr[i];
		}
	}


	if((int) weight.size() != g)
	{
		THROW("LoadWeights: expected array of length g for g = " << g << ", got " << weight.size() << "!");
	}

	for(int i = 0; i < g; i++)
	{
		Weight[i] = weight[i];
	}
}

void GRAPH::InitStrategyAdj()
{
	for(int i = 0; i < q; i++)
	{
		S.OutDegree[i] = 0;
		S.InDegree[i] = 0;
	}

	for(int i = 0; i < q; i++)
	{
		for(int j = 0; j < q; j++)
		{
			if(S.Prob[i][j] > 0.)
			{
				S.Succ[i][S.OutDegree[i]++] = (ADJ) {j};
				S.Pred[j][S.InDegree[j]++]  = (ADJ) {i};
			}
		}
	}
}


void GRAPH::FindReachableStates()
{
	for(int i = 0; i < q; i++)
	{
		S.IsReachable[i] = 0;
	}

	int v = 0;
	while(!(S.OutDegree[v]))
	//we start in the first state which has a successor
	{
		v++;
	}

	QBeg = 0;
	QEnd = 0;
	Q[QEnd++] = v;
	S.IsReachable[v] = 1;

	while(QBeg < QEnd)
	{
		int cur = Q[QBeg++];
		for(int k = 0; k < S.OutDegree[cur]; k++)
		{
			int s = S.Succ[cur][k].To;
			if(S.IsReachable[s])
			//already visited
			{
				continue;
			}

			Q[QEnd++] = s;
			S.IsReachable[s] = 1;
		}
	}
}

void GRAPH::Check_dList()
{
	if(!(dListSize))
	{
		THROW("The obtained list of window lengths is empty!");
	}

	for(int i = 1; i < dListSize; i++)
	{
		if((*p_dList)[i] <= (*p_dList)[i - 1])
		{
			THROW("The list of window lengths must be in increasing order!");
		}
	}
}

struct FREQ_VECTOR
{
	int LastState;
	std::vector<int> FreqVector;

	int g() const {return FreqVector.size();}

	bool operator < (const FREQ_VECTOR& v) const
	{
		if(LastState != v.LastState)
		{
			return LastState < v.LastState;
		}

		for(int t = 0; t < g(); t++)
		{
			if(FreqVector[t] != v.FreqVector[t])
			{
				return FreqVector[t] < v.FreqVector[t];
			}
		}
	}

	bool operator == (const FREQ_VECTOR& v) const
	{
		if(LastState != v.LastState)
		{
			return 0;
		}

		for(int t = 0; t < g(); t++)
		{
			if(FreqVector[t] != v.FreqVector[t])
			{
				return 0;
			}
		}

		return 1;
	}

	struct HASHER
	{
		std::size_t operator () (const FREQ_VECTOR& v) const
		{
			typedef unsigned long long uhuge;
			uhuge a = v.LastState;
			for(int t = 0; t < v.g(); t++)
			{
				a ^= v.FreqVector[t] << (((t + 1) * 3) & 63);
			}

			return std::hash<uhuge>()(a);
		}
	};

	int GetCount(int t) const {return FreqVector[t];}
	void IncCount(int t) {FreqVector[t]++;}
	int GetState() const {return LastState;}
	void SetState(int i) {LastState = i;}

	FREQ_VECTOR(int g): LastState(-1), FreqVector(g) {}
	FREQ_VECTOR(const FREQ_VECTOR&) = default;
	FREQ_VECTOR(const FREQ_VECTOR& Prev, int i, int t): FREQ_VECTOR(Prev)
	{
		SetState(i);
		if(t >= 0) IncCount(t);
	}
};


void GRAPH::ComputeValue()
{
	for(int i = 0; i < q; i++)
	{
		if(S.IsReachable[i])
		{
			DistrPenalty[i].clear();
			DistrPenalty[i].resize(dListSize);
			CurDistrPenalty = &(DistrPenalty[i][0]);

			if(ComputationType == DFS)
			{
				std::vector<int> fv(g);
				if((UseVertexSetting) && (Q2T[i] >= 0)) fv[Q2T[i]]++;
				Search_DFS(i, 1., UseVertexSetting, fv, 0);
			}
			else //ComputationType == DP
			{
				Search_DP(i);
			}
		}
	}
}

void GRAPH::Search_DFS(int i, double p, int dCur, const std::vector<int>& FreqVector, int di)
{
	if(dCur == (*p_dList)[di])
	{
		for(int t = 0; t < g; t++)
		{
			CurFreq[t] = FreqVector[t] / double(dCur);
		}

		double penalty = GetCurrentPenalty();
		CurDistrPenalty[di] += penalty * p;

		if(++di == dListSize)
		{
			return;
		}
	}

	for(int k = 0; k < S.OutDegree[i]; k++)
	{
		int j = S.Succ[i][k].To;
		std::vector<int> fv(FreqVector);
		if(Q2T[j] >= 0) fv[Q2T[j]]++;
		Search_DFS(j, p * S.Prob[i][j], dCur + 1, fv, di);
	}
}

void GRAPH::Search_DP(int InitialState)
{
	typedef std::unordered_map<FREQ_VECTOR, double, typename FREQ_VECTOR::HASHER> MAP;
	MAP Map1, Map2;
	MAP* CurMap = &Map1;
	MAP* NextMap = &Map2;

	FREQ_VECTOR v0(FREQ_VECTOR(g), InitialState, (UseVertexSetting) ? Q2T[InitialState] : -1);
	(*CurMap)[v0] = 1.;

	int di = 0;
	for(int dd = UseVertexSetting; 1; dd++)
	//could be further optimized by disregarding the final state for dd == dMax - 1
	{
		if(dd == (*p_dList)[di])
		{
			for(auto& [v, p]: *CurMap)
			{
				for(int t = 0; t < g; t++)
				{
					CurFreq[t] = v.GetCount(t) / double(dd);
				}

				double penalty = GetCurrentPenalty();
				CurDistrPenalty[di] += penalty * p;
			}

			if(++di == dListSize)
			{
				return;
			}
		}

		for(auto& [v, p]: *CurMap)
		{
			int i = v.GetState();
			for(int k = 0; k < S.OutDegree[i]; k++)
			{
				int j = S.Succ[i][k].To;
				FREQ_VECTOR v2(v, j, Q2T[j]);
				(*NextMap)[v2] += p * S.Prob[i][j];
			}
		}

		std::swap(CurMap, NextMap);
		NextMap->clear();
	}
}

double GRAPH::GetCurrentPenalty()
{
	double rsl = 0.;
	if(MetricUsed >= FIRST_HARD)
	{
		for(int t = 0; t < g; t++)
		{
			const double eps = 1e-6;
			if((CurFreq[t] < WantedLowerFreq[t] - eps) || (CurFreq[t] > WantedUpperFreq[t] + eps))
			{
				if(MetricUsed == HardMax)
				{
					Maximize(rsl, Weight[t]);
				}
				else //MetricUsed == HardSum
				{
					rsl += Weight[t];
				}
			}
		}
	}
	else
	{
		double(* PenaltyFunc)(double, double) = (MetricUsed == L1) ? PenaltyFuncL1 : PenaltyFuncL2Squared;

		for(int t = 0; t < g; t++)
		{
			rsl += Weight[t] * PenaltyFunc(CurFreq[t], WantedFreq[t]);
		}

		if(MetricUsed == L2)
		{
			rsl = sqrt(rsl);
		}
	}

	return rsl;
}

/*static*/ double GRAPH::PenaltyFuncL1(double a, double b)
{
	return std::abs(a - b);
}

/*static*/ double GRAPH::PenaltyFuncL2Squared(double a, double b)
{
	double diff = a - b;
	return diff * diff;
}

} //namespace STABILITY
