#ifndef UTILS_H
#define UTILS_H

#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>
#include <vector>
#include <unordered_map>
#include <algorithm>
#include <tuple>
#include <sstream>

#define THROW(s) {std::stringstream str; str << s; throw std::runtime_error(str.str());}

typedef std::pair<int, int> UNIT_EDGE_TYPE;
typedef std::tuple<int, int, double> GENERAL_EDGE_TYPE;


template<typename T>
void Minimize(T& a, T b)
//assigns b to a provided the new value is less
{
	if(b < a)
	{
		a = b;
	}
}

template<typename T>
void Maximize(T& a, T b)
//assigns b to a provided the new value is greater
{
	if(b > a)
	{
		a = b;
	}
}



#define MAXN 0 //just an identifier - do not change!
#define MAXQ 1 //just an identifier - do not change!
#define MAXG 2 //just an identifier - do not change!
#define MAXQG 3 //just an identifier - do not change!


struct PAR_MANAGER
{
	union
	{
		int par[4];
		struct
		{
			int n, q, g, qg;
		};
	};

	void* pBegin = NULL;
	void* pEnd;

	void Init(void* p, int nb, const std::vector<int>& q2t)
	//from STABILITY
	{
		if(pBegin)
		{
			THROW("Trying to create two instances of GRAPH in parallel!");
		}

		pBegin = p;
		pEnd = ((char*) p) + nb;
		q = q2t.size();
		g = -1;
		for(int i = 0; i < q; i++)
		{
			Maximize(g, q2t[i]);
		}

		g++;
	}

	void Init(void* p, int nb, int n)
	//from SCCs
	{
		if(pBegin)
		{
			THROW("Trying to create two instances of GRAPH in parallel!");
		}

		pBegin = p;
		pEnd = ((char*) p) + nb;
		this->n = n;
	}

	void InitFinished()
	{
		pBegin = NULL;
	}
}
ParManager;

template<typename T, int S>
struct ARRAY1D
{
	int size = 0;
	T* a = NULL;

	ARRAY1D()
	{
		if(!((this >= ParManager.pBegin) && (this < ParManager.pEnd)))
		{
			THROW("Trying to create an array outside the current instance of GRAPH!");
		}
		
		size = ParManager.par[S];
		a = new T[size]();
	}

	~ARRAY1D()
	{
		delete[] a;
	}

	ARRAY1D& operator = (ARRAY1D& other)
	{
		for(int i = 0; i < size; i++)
		{
			(*this)[i] = other[i];
		}

		return *this;
	}

	T& operator [] (int i)
	{
		return a[i];
	}
};

template<typename T, int S1, int S2>
struct ARRAY2D
{
	int size1 = 0;
	int size2 = 0;
	T* a = NULL;

	ARRAY2D()
	{
		if(!((this >= ParManager.pBegin) && (this < ParManager.pEnd)))
		{
			THROW("Trying to create an array outside the current instance of GRAPH!");
		}
		
		size1 = ParManager.par[S1];
		size2 = ParManager.par[S2];
		a = new T[size1 * size2];
	}

	~ARRAY2D()
	{
		delete[] a;
	}

	ARRAY2D& operator = (ARRAY2D& other)
	{
		for(int i = 0; i < size1; i++)
		{
			for(int j = 0; j < size2; j++)
			{
				(*this)[i][j] = other[i][j];
			}
		}

		return *this;
	}

	T* operator [] (int i)
	{
		return a + (i * size2);
	}
};

template<typename T, int S1, int S2, int S3>
struct ARRAY3D
{
	int size1 = 0;
	int size2 = 0;
	int size3 = 0;
	T* a = NULL;

	ARRAY3D()
	{
		if(!((this >= ParManager.pBegin) && (this < ParManager.pEnd)))
		{
			THROW("Trying to create an array outside the current instance of GRAPH!");
		}
		
		size1 = ParManager.par[S1];
		size2 = ParManager.par[S2];
		size3 = ParManager.par[S3];
		a = new T[size1 * size2 * size3];
	}

	~ARRAY3D()
	{
		delete[] a;
	}

	ARRAY3D& operator = (ARRAY3D& other)
	{
		for(int i = 0; i < size1; i++)
		{
			for(int j = 0; j < size2; j++)
			{
				for(int k = 0; k < size3; k++)
				{
					(*this)[i][j][k] = other[i][j][k];
				}
			}
		}

		return *this;
	}

	struct HELPER
	{
		ARRAY3D* p;
		int i;

		HELPER(ARRAY3D* p, int i): p(p), i(i) {}
		T* operator [] (int j) {return p->a + (((i * p->size2) + j) * p->size3);}
	};

	HELPER operator [] (int i)
	{
		return HELPER(this, i);
	}
};

#endif //UTILS_H
