#include <torch/extension.h>
#include <ATen/ATen.h>

#include "stability.h"
#include "SCCs.h"

using namespace pybind11::literals;

PYBIND11_MODULE(TORCH_EXTENSION_NAME, m)
{
    m.doc() = "Plugin for fast strategy evaluation";


    py::class_<STABILITY::GRAPH> StabilityInstance(m, "CppStabilityEvaluator");

    py::enum_<STABILITY::GRAPH::METRIC_TAG>(StabilityInstance, "MetricTag")
        .value("L1", STABILITY::GRAPH::METRIC_TAG::L1)
        .value("L2", STABILITY::GRAPH::METRIC_TAG::L2)
        .value("L2Squared", STABILITY::GRAPH::METRIC_TAG::L2Squared)
        .value("HardMax", STABILITY::GRAPH::METRIC_TAG::HardMax)
        .value("HardSum", STABILITY::GRAPH::METRIC_TAG::HardSum);

    py::enum_<STABILITY::GRAPH::COMPUTATION_TAG>(StabilityInstance, "ComputationTag")
        .value("DFS", STABILITY::GRAPH::COMPUTATION_TAG::DFS)
        .value("DP", STABILITY::GRAPH::COMPUTATION_TAG::DP);

    StabilityInstance
        .def(py::init<const std::vector<int>&, STABILITY::GRAPH::METRIC_TAG, const std::vector<double>&,
             const std::vector<double>&, bool, STABILITY::GRAPH::COMPUTATION_TAG>(),
            "q2t: list of ints, its i-th entry is the number of the target corresponding to state i, or -1 for no target\n"
            "metric: either one of the soft metrics: L1, L2, L2Squared; or one of the hard metrics: HardMax, HardSum\n"
            "wanted_distr: for a soft metric, the wanted (sub)distribution; for a hard metric, it's twice as long "
            "and contains lower bounds in the first half and upper bounds in the second half\n",
            "q2t"_a,
            "metric"_a,
            "wanted_distr"_a,
            "weights"_a,
            "count_initial_vertex"_a = 1,
            "computation"_a = STABILITY::GRAPH::COMPUTATION_TAG::DP)
        .def("eval",
            &STABILITY::GRAPH::PyEvaluateStrategy,
            "Evaluates a given strategy tensor with respect to a list of window lengths, which must be given in increasing order.\n"
            "The strategy is assumed to consist of a single SCC and the states outside the SCC should have all outgoing probabilities set to 0.\n"
            "Returns a tensor whose [i][j] entry is the expected penalty for the i-th window length "
            "when starting from state j, or -1 if the state is unreachable",
            "strategy"_a,
            "window_lengths_list"_a);


    py::class_<SCCs::EVAL>(m, "CppSCCsEvaluator")
        .def(py::init<int>(),
                "Initializes the number of vertices",
            "n"_a)
        .def("get_all_sccs",
            &SCCs::EVAL::PyGetAllSCCs,
            "Returns all strongly connected components as a 2D tensor:\n"
            "The [i][j] entry says whether the j-th vertex lies in the i-th SCC.",
            "strategy"_a)
        .def("get_bottom_sccs",
            &SCCs::EVAL::PyGetBottomSCCs,
            "Returns all bottom strongly connected components as a 2D tensor:\n"
            "The [i][j] entry says whether the j-th vertex lies in the i-th BSCC.",
            "strategy"_a);
}
