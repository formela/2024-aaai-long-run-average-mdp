#include "utils.h"

namespace SCCs
{
struct STACKITEM
{
	int Vertex;
	int Index;
};

struct EVAL
{
	struct PAR_MANAGER_CALLER
	{
		PAR_MANAGER_CALLER(EVAL* E, int n)
		{
			ParManager.Init(E, sizeof(EVAL), n);
		}
	}
	PMC;

	EVAL(int n): PMC(this, n), n(n)
	{
		ParManager.InitFinished();
	}

	at::Tensor /*int[][N]*/ PyGetAllSCCs(at::Tensor /*double[N][N]*/ strat);
	at::Tensor /*int[][N]*/ PyGetBottomSCCs(at::Tensor /*double[N][N]*/ strat);

	void LoadStrategy(at::Tensor /*double[N][N]*/ strat);

	void ComputeSCCs();
	void MarkBottomSCCs();

	int n;
	ARRAY1D<int, MAXN> OutDegree;
	ARRAY2D<int, MAXN, MAXN> Succ;

	ARRAY1D<int, MAXN> Comp;

	ARRAY1D<int, MAXN> CompStack;
	int nCompStack;

	ARRAY1D<STACKITEM, MAXN> Stack;
	int nStack;

	ARRAY1D<int, MAXN> Order;
	ARRAY1D<int, MAXN> Least;

	ARRAY1D<int, MAXN> CompToBottomID;
	ARRAY1D<int, MAXN> BottomComp;

	int nVisited;
	int nComps;
	int nBottomComps;
};

void EVAL::LoadStrategy(at::Tensor /*double[N][N]*/ strat)
{
	if(strat.dim() != 2)
	{
		THROW("LoadStrategy: n*n array expected, got " << strat.dim() << " dimension(s)!");
	}

	if((strat.size(0) != n) || (strat.size(1) != n))
	{
		THROW("LoadStrategy: n*n array expected for n = " << n << ", got " \
		      << strat.size(0) << "*" << strat.size(1) << "!");
	}

	double* p = strat.data_ptr<double>();
	for(int i = 0; i < n; i++)
	{
		int& d = OutDegree[i];
		d = 0;
		for(int j = 0; j < n; j++)
		{
			if(*(p++) > 0.)
			{
				Succ[i][d++] = j;
			}
		}
	}
}

at::Tensor /*int[][N]*/ EVAL::PyGetAllSCCs(at::Tensor /*double[N][N]*/ strat)
{
	LoadStrategy(strat);
	ComputeSCCs();

	at::Tensor rsl = torch::zeros({nComps, n}, torch::kInt32);
	int* p = rsl.data_ptr<int>();

	for(int i = 0; i < n; i++)
	{
		p[(Comp[i] * n) + i] = 1;
	}

	return rsl;
}

at::Tensor /*int[][N]*/ EVAL::PyGetBottomSCCs(at::Tensor /*double[N][N]*/ strat)
{
	LoadStrategy(strat);
	ComputeSCCs();
	MarkBottomSCCs();

	at::Tensor rsl = torch::zeros({nBottomComps, n}, torch::kInt32);
	int* p = rsl.data_ptr<int>();

	for(int i = 0; i < n; i++)
	{
		if(BottomComp[i] != -1)
		{
			p[(BottomComp[i] * n) + i] = 1;
		}
	}

	return rsl;
}

void EVAL::MarkBottomSCCs()
{
	for(int i = 0; i < nComps; i++)
	{
		CompToBottomID[i] = -2; //undetermined ID yet
	}

	for(int i = 0; i < n; i++)
	{
		for(int k = 0; k < OutDegree[i]; k++)
		{
			int j = Succ[i][k];
			if(Comp[j] != Comp[i])
			{
				CompToBottomID[Comp[i]] = -1; //not a BSCC, thus no ID
			}
		}
	}

	nBottomComps = 0;
	for(int i = 0; i < nComps; i++)
	{
		if(CompToBottomID[i] == -2)
		{
			CompToBottomID[i] = nBottomComps++;
		}
	}

	for(int i = 0; i < n; i++)
	{
		BottomComp[i] = CompToBottomID[Comp[i]];
	}
}

void EVAL::ComputeSCCs()
{
	nStack = 0;
	nCompStack = 0;
	nComps = 0;

	for(int i = 0; i < n; i++)
	{
		Comp[i] = -1;
	}

	for(int i = 0; i < n; i++)
	{
		if(Comp[i] != -1)
		{
			continue;
		}

		Stack[nStack++] = {i, 0};
		CompStack[nCompStack++] = i;
		Comp[i] = -2;
		Order[i] = nVisited++;
		Least[i] = Order[i];

		while(nStack)
		{
			//printf("%d, %d\n", i, nStack);
			STACKITEM Cur = Stack[nStack - 1];
			if(Cur.Index == OutDegree[Cur.Vertex])
			//going up
			{
				nStack--;
				if(nStack)
				{
					Stack[nStack - 1].Index++;

					if(Least[Stack[nStack - 1].Vertex] > Least[Cur.Vertex])
					{
						Least[Stack[nStack - 1].Vertex] = Least[Cur.Vertex];
					}
				}

				if(Least[Cur.Vertex] == Order[Cur.Vertex])
				//the component is complete
				{
					while(1)
					{
						Comp[CompStack[--nCompStack]] = nComps;
						if(CompStack[nCompStack] == Cur.Vertex)
						{
							break;
						}
					}

					nComps++;
				}

				continue;
			}

			int t = Succ[Cur.Vertex][Cur.Index];
			if(Comp[t] == -1)
			//a new vertex
			{
				Stack[nStack++] = {t, 0};
				CompStack[nCompStack++] = t;
				Comp[t] = -2;
				Order[t] = nVisited++;
				Least[t] = Order[t];
			}
			else
			{
				if(Comp[t] == -2)
				//in the component stack
				{
					if(Order[t] < Least[Cur.Vertex])
					{
						Least[Cur.Vertex] = Order[t];
					}
				}

				Stack[nStack - 1].Index++;
			}
		}
	}
}

} //namespace SCCs
