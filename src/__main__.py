# Standard Imports
import random

# Third-party imports
import hydra
from hydra.core.hydra_config import HydraConfig
from omegaconf import DictConfig, OmegaConf, open_dict

# Local imports
from src.io.logging import get_logger
from src.launch import AbstractTask

logger = get_logger()

OmegaConf.register_new_resolver(
    'common_seed', lambda: random.randint(0, 2 ** 31), use_cache=True, replace=True
)
OmegaConf.register_new_resolver(
    'get_mode', lambda x: hydra.utils.get_object(x).mode, replace=True
)


def replace_key_recursive(x: dict, old: str, new: str):
    if old in x:
        x[new] = x.pop(old)
    for k, v in x.items():
        if isinstance(v, dict):
            replace_key_recursive(v, old, new)
    return x


def target_to_class(x: DictConfig) -> DictConfig:
    x = OmegaConf.to_container(x, resolve=True)
    replace_key_recursive(x, '_target_', 'class')
    return x


OmegaConf.register_new_resolver(
    'target_to_class', target_to_class, replace=True
)


@hydra.main(version_base=None, config_path="../conf", config_name="default")
def app(cfg: DictConfig) -> None:
    hydra_cfg = HydraConfig.get()

    # OmegaConf.save(
    #     config=cfg.task,
    #     f=f'{hydra_cfg.run.dir}/{hydra_cfg.output_subdir}/task_resolved.yaml',
    #     resolve=True,
    # )

    hydra.utils.call(cfg.logging)

    task: AbstractTask = hydra.utils.instantiate(
        cfg.task,
        _recursive_=True,
        _convert_='all'
    )
    # todo: too hacky
    task.report.writer.add('settings', OmegaConf.to_container(cfg.task, resolve=True))
    task.execute()


if __name__ == '__main__':
    app()
