from itertools import product
from logging import Logger
from typing import List, Tuple

import numpy as np
import torch
from termcolor import colored

from src.model.utils import expand_graph_mask, aug_nodes2strings

from src.io import logging
logger = logging.get_logger('strategy')


class MatrixStrategy(torch.nn.Module):
    def __init__(
            self,
            nodes_n: int,
            graph_matrix: torch.Tensor,
            aug_nodes: List[Tuple[Tuple[Tuple[int, int], ...], int]],  # [(((n1, m1), (n2, m2), ...), m_g1), ...]
            num_of_agents: int,
            rounding_threshold: float,
            node_names: List = None,
            loader=None,
            loader_params=None,
            independent_agents: bool = False,
            device='cpu',
            data_type=torch.float64,
    ):
        super(MatrixStrategy, self).__init__()

        self.nodes_n = nodes_n
        self.data_type = data_type
        self.num_of_agents = num_of_agents
        self.independent_agents = independent_agents
        self.threshold = rounding_threshold
        self.device = device
        self.aug_nodes = aug_nodes
        self.node_names = node_names

        self.register_buffer('mask', expand_graph_mask(graph_matrix, aug_nodes))

        if loader is not None:
            strategy = strategy_loaders[loader](**loader_params)
            logger.info(strategy)
            assert strategy.size() == self.mask_size
            params = self.mask * torch.log(strategy + rounding_threshold / 2)
        else:
            params = self._init_params()
        self.params = torch.nn.Parameter(params)

    def _init_params(self):
        if self.independent_agents:
            shape = (self.num_of_agents,) + self.mask.shape
            return self.mask * torch.log(
                torch.rand(shape, dtype=self.data_type, requires_grad=True, device=self.device)
            )
        else:
            return self.mask * torch.log(
                torch.rand_like(self.mask, dtype=self.data_type, requires_grad=True, device=self.device)
            )

    def reset_parameters(self):
        self.params.data = self._init_params()

    def schedule(self, total_steps, current_step):
        pass

    def _combine_strategies(self, strategy_matrices: List[torch.Tensor]):
        """strategy: (num_of_agents, N, N)"""
        combined = strategy_matrices[0]
        for i in range(1, self.num_of_agents):
            combined = torch.kron(combined, strategy_matrices[i])
        return combined

    def get_mask(self):
        return self.mask

    def get_full_mask(self):
        if self.independent_agents:
            return self._combine_strategies([self.mask] * self.num_of_agents)
        else:
            return self.mask

    def forward(self):
        x = torch.exp(self.params) * self.mask
        strategy = torch.nn.functional.normalize(x, p=1., dim=-1)
        if not self.training:
            x = torch.threshold(strategy, threshold=self.threshold, value=0.0)
            strategy = torch.nn.functional.normalize(x, p=1., dim=-1)
        if self.independent_agents:
            return self._combine_strategies(strategy)
        else:
            return strategy

    def stringify(self, style: str = 'matrix', precision: int = 3, times: torch.Tensor = None, delay: torch.Tensor = None):
        """
        Stringify strategy with given style: 'label', 'matrix', 'graphviz'.
        label: stringify strategy as a list of edges with probabilities and lengths
        matrix: stringify strategy as a matrix
        graphviz: stringify strategy as a graphviz graph

        @param style: Style of printing: 'label', 'matrix', 'graphviz'
        @param precision: Number of digits after decimal point
        @param times: Matrix of edge lengths
        @param delay: Matrix of edge delay
        """

        print_delay = delay is not None
        print_len = times is not None

        strategy_str = ''
        strategy = self()

        if style == 'matrix':
            with np.printoptions(precision=precision, suppress=True, threshold=np.inf, linewidth=np.inf):
                strategy_str += colored('Strategy:\n', 'blue')
                strategy_str += np.array2string(strategy.detach().numpy()) + '\n'
                if print_len:
                    strategy_str += colored('Length:\n', 'blue')
                    strategy_str += np.array2string(times.detach().numpy()) + '\n'
                if print_delay:
                    strategy_str += colored('Delay:\n', 'blue')
                    strategy_str += np.array2string(delay.detach().numpy()) + '\n'
        else:
            if self.node_names:
                aug_node_names = aug_nodes2strings(self.aug_nodes, self.node_names)
            else:
                aug_node_names = aug_nodes2strings(self.aug_nodes, list(range(self.nodes_n)))
            for i, j in product(range(strategy.shape[0]), range(strategy.shape[1])):
                source_index = i
                destination_index = j
                source = aug_node_names[source_index]
                destination = aug_node_names[destination_index]
                prob = strategy[source_index, destination_index]
                # edge_delay = delay[source_index, destination_index] if print_delay else 0
                if prob > 0:
                    if style == 'label':
                        edge_str = f'{source}--{destination}'
                    elif style == 'graphviz':
                        edge_str = f'"{source}"->"{destination}"'
                    elif style == 'index':
                        edge_str = f'{source_index:5.0f}--{destination_index:3.0f}  '
                    else:
                        raise AttributeError(f'unknown printing style "{style}"')

                    label = f'{prob:.{precision}f}'
                    length = times[source_index, destination_index] if print_len else 0
                    edge_delay = delay[source_index, destination_index] if print_delay else 0
                    label += f' {length:.{precision}f}' * print_len
                    label += f' +' * print_len * print_delay
                    label += f' {edge_delay:.{precision}f}' * print_delay

                    if style != 'graphviz':
                        strategy_str += f'\n{edge_str}:  {label}'
                    else:
                        strategy_str += f'\n  {edge_str}[label="{label}"]'
        return strategy_str

    def print(
            self,
            logger: Logger,
            style: str = 'label',
            precision: int = 3,
            times: torch.Tensor = None,
            delay: torch.Tensor = None,
            level: int = logging.INFO
    ):
        """
        Print strategy to logger with given style: 'label', 'matrix', 'graphviz'.
        label: print strategy as a list of edges with probabilities and lengths
        matrix: print strategy as a matrix
        graphviz: print strategy as a graphviz graph

        @param logger: Logger object
        @param style: Style of printing: 'label', 'matrix', 'graphviz'
        @param precision: Number of digits after decimal point
        @param times: Matrix of edge lengths
        @param delay: Matrix of edge delays
        @param level: Logging level
        """
        if level < logger.level:
            return
        logger.log(level, self.stringify(style, precision, times, delay))


def load_strategy_file(path):
    strategy = np.load(path, allow_pickle=True)
    return torch.as_tensor(strategy, dtype=torch.float64)


def load_strategy_mongodb(host, db, run_id, username, password):
    import pymongo
    import pickle
    from bson.objectid import ObjectId
    collection = pymongo.MongoClient(host=host, username=username, password=password)[db]['runs']
    run = collection.find_one({'_id': ObjectId(run_id)}, {'strategy': 1})
    strategy = pickle.loads(run.get('strategy'))
    return torch.as_tensor(strategy, dtype=torch.float64)


def load_strategy_nparray(array):
    return torch.as_tensor(array, dtype=torch.float64)


strategy_loaders = {
    'file': load_strategy_file,
    'mongodb': load_strategy_mongodb,
    'array': load_strategy_nparray,
}
