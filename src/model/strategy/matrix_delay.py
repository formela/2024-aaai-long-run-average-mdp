import torch
from src.model.strategy.matrix_strategy import strategy_loaders


class Delay(torch.nn.Module):
    def __init__(self, mask, rounding_threshold=0, training_start=0, lr=None, loader=None, loader_params=None, indep_agents=False):
        super().__init__()
        self.mask = mask
        self.lr = lr
        self.rounding_threshold = rounding_threshold
        self.training_start = training_start
        if loader is not None:
            delays = strategy_loaders[loader](**loader_params)
            assert delays.size() == self.mask.size()
            params = self.mask * torch.log(delays + rounding_threshold / 2)
        else:
            params = self._init_params()
        self.params = torch.nn.Parameter(params)

    def _init_params(self):
        # think about proper inits
        return self.mask * torch.rand_like(self.mask, dtype=torch.float64) * self.rounding_threshold

    def reset_parameters(self):
        self.params.data = self._init_params()

    def schedule(self, total_steps, current_step):
        if current_step > total_steps * self.training_start:
            self.params.requires_grad = True
        else:
            self.params.requires_grad = False

    def forward(self):
        small_params = torch.tensor(0.0)
        if self.rounding_threshold > 0:
            x = torch.exp(self.params / self.rounding_threshold - 1.0)
            x = torch.min(torch.tensor(1.0), x)
            small_params = self.rounding_threshold * x
        delay = torch.max(self.params, small_params) * self.mask
        delay = RoundDown.apply(delay, self.rounding_threshold)
        return delay


class RoundDown(torch.autograd.Function):
    @staticmethod
    def forward(ctx, x, base):
        if base == 0:
            return x
        else:
            return base * torch.floor(x / base)

    @staticmethod
    def backward(ctx, x_grad):
        return x_grad, None
