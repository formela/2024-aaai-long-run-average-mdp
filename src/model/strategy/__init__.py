import torch
import numpy as np

# Local Imports
from src.model.strategy.matrix_strategy import MatrixStrategy
from src.model.strategy.matrix_delay import Delay
from src.io.logging import get_logger
logger = get_logger('strategy')


class Strategy(torch.nn.Module):
    def __init__(self, graph, rounding_threshold, loader=None, loader_params=None, indep_agents=False):
        super().__init__()
        self.threshold = rounding_threshold
        if indep_agents and graph.num_of_agents > 1:
            self.num_of_agents = graph.num_of_agents
            self.register_buffer('mask', graph.mask_before_ma[None, ...])
            self.mask_size = torch.Size((self.num_of_agents, *graph.mask_before_ma.size()))
        else:
            self.num_of_agents = 1
            self.register_buffer('mask', graph.mask)
            self.mask_size = self.mask.size()
        if loader is not None:
            strategy = strategy_loaders[loader](**loader_params)
            logger.info(strategy)
            assert strategy.size() == self.mask_size
            params = self.mask * torch.log(strategy + rounding_threshold / 2)
        else:
            params = self._init_params()
        self.params = torch.nn.Parameter(params)

    def _init_params(self):
        return self.mask * torch.log(torch.rand(size=self.mask_size, dtype=torch.float64))

    def reset_parameters(self):
        self.params.data = self._init_params()

    def schedule(self, total_steps, current_step):
        pass

    def combine_strategies(self, strategy):
        n = self.num_of_agents
        w = strategy[0].shape[0]
        comb_strat = strategy[0].reshape([w] + [1] * (n-1) + [w] + [1] * (n-1))
        for i in range(1, n):
            comb_strat = comb_strat * strategy[i].reshape([1] * i + [w] + [1] * (n-1) + [w] + [1] * (n-1-i))
        return torch.flatten(torch.flatten(comb_strat, 0, self.num_of_agents-1), -self.num_of_agents, -1)

    def forward(self):
        x = torch.exp(self.params) * self.mask
        strategy = torch.nn.functional.normalize(x, p=1., dim=-1)
        if not self.training:
            x = torch.threshold(strategy, threshold=self.threshold, value=0.0)
            strategy = torch.nn.functional.normalize(x, p=1., dim=-1)
        if self.num_of_agents == 1:
            return strategy
        return self.combine_strategies(strategy)


class Delay(torch.nn.Module):
    def __init__(self, mask, rounding_threshold=0, training_start=0, lr=None):
        super().__init__()
        self.mask = mask
        self.lr = lr
        self.rounding_threshold = rounding_threshold
        self.training_start = training_start
        self.params = torch.nn.Parameter(self._init_params())

    def _init_params(self):
        # think about proper inits
        return self.mask * torch.rand_like(self.mask, dtype=torch.float64) * self.rounding_threshold

    def reset_parameters(self):
        self.params.data = self._init_params()

    def schedule(self, total_steps, current_step):
        if current_step > total_steps * self.training_start:
            self.params.requires_grad = True
        else:
            self.params.requires_grad = False

    def forward(self):
        small_params = torch.tensor(0.0)
        if self.rounding_threshold > 0:
            x = torch.exp(self.params / self.rounding_threshold - 1.0)
            x = torch.min(torch.tensor(1.0), x)
            small_params = self.rounding_threshold * x
        delay = torch.max(self.params, small_params) * self.mask
        delay = RoundDown.apply(delay, self.rounding_threshold)
        return delay


class RoundDown(torch.autograd.Function):
    @staticmethod
    def forward(ctx, x, base):
        if base == 0:
            return x
        else:
            return base * torch.floor(x / base)

    @staticmethod
    def backward(ctx, x_grad):
        return x_grad, None


def load_strategy_file(path):
    strategy = np.load(path, allow_pickle=True)
    return torch.as_tensor(strategy, dtype=torch.float64)


def load_strategy_mongodb(host, db, run_id, username, password):
    import pymongo
    import pickle
    from bson.objectid import ObjectId
    collection = pymongo.MongoClient(host=host, username=username, password=password)[db]['runs']
    run = collection.find_one({'_id': ObjectId(run_id)}, {'strategy': 1})
    strategy = pickle.loads(run.get('strategy'))
    return torch.as_tensor(strategy, dtype=torch.float64)


def load_strategy_nparray(array):
    return torch.as_tensor(array, dtype=torch.float64)


strategy_loaders = {
    'file': load_strategy_file,
    'mongodb': load_strategy_mongodb,
    'array': load_strategy_nparray,
}
