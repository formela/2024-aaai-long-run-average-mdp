# Standard Imports
from abc import ABC, abstractmethod

# Third-party Imports
import torch


class HardMin(torch.nn.Module):
    def forward(self, x):
        return torch.min(x)


class HardMax(torch.nn.Module):
    def forward(self, x):
        return torch.max(x)


class PNorm(torch.nn.Module):
    def __init__(self, ord):
        torch.nn.Module.__init__(self)
        self.ord = ord

    def forward(self, x):
        x = torch.flatten(x)
        x = torch.sum(torch.abs(x ** self.ord))
        return x


class PNormClamp(ABC, torch.nn.Module):
    def __init__(self, eps, ord, dim=-1):
        torch.nn.Module.__init__(self)
        self.eps = eps
        self.ord = ord
        self.dim = dim

    @abstractmethod
    def forward(self, x):
        ...


class PNormClampMin(PNormClamp):
    def forward(self, x):
        min_x = x.max(dim=self.dim, keepdim=True).values
        min_x_static = min_x.detach()
        eps = self.eps * min_x_static
        x = (-x + min_x_static + eps) / eps
        x = torch.clamp(x, min=0.0)
        x = -(x ** self.ord).sum(dim=self.dim)
        return x


class PNormClampMinPositive(PNormClamp):
    def forward(self, x):
        min_x = x.amax(dim=self.dim, keepdim=True)
        m = min_x.detach()
        # TODO: possible division by zero i f m == 0.0
        eps_m = self.eps * m
        new_max = m * (1 + self.eps)
        x = torch.clamp(x, max=new_max)
        x = new_max - eps_m * ((1 + 1 / self.eps - x / eps_m) ** self.ord)
        return x.sum(dim=self.dim)


class PNormClampMax(PNormClamp):
    def forward(self, x):
        max_x = x.max(dim=self.dim, keepdim=True).values
        max_x_static = max_x.detach()
        eps = self.eps * max_x_static
        x = (x - max_x_static + eps) / eps
        x = torch.clamp(x, min=0.0)
        x = (x ** self.ord).sum(dim=self.dim)
        return x


class LogSumExpClampLoss(torch.nn.Module):
    def __init__(self, eps, tau):
        super().__init__()
        self.eps = eps
        self.tau = tau

    def forward(self, steal):
        steal = torch.flatten(steal)
        max_steal = torch.max(steal).detach()
        eps = self.eps * max_steal
        steal = torch.threshold(steal, threshold=max_steal - eps, value=0.0)
        steal = steal[steal.nonzero(as_tuple=True)]
        loss = torch.logsumexp(steal * self.tau, dim=0) / self.tau
        return loss


class LogSumExpLoss(torch.nn.Module):
    def __init__(self, tau):
        super().__init__()
        self.tau = tau

    def forward(self, steal):
        steal = torch.flatten(steal)
        loss = torch.logsumexp(steal * self.tau, dim=0) / self.tau
        return loss


class SoftMaxLoss(torch.nn.Module):
    def __init__(self, tau):
        super().__init__()
        self.tau = tau

    def forward(self, steal):
        steal = torch.flatten(steal)
        softmax_steal = torch.softmax(steal * self.tau, dim=0)
        loss = torch.sum(steal * softmax_steal, dim=0)
        return loss
