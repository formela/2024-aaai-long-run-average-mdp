# Third-party Imports
import torch
from termcolor import colored

# Local Imports
from src.io.logging import get_logger

logger = get_logger('objective')


def solve_system(A, b):
    try:
        solution = torch.linalg.solve(A, b)
    except RuntimeError as error_msg:
        logger.error('torch.linalg.solve failed with runtime error: error_msg')
        logger.debug(colored('A:', 'blue') + '\n', A)
        logger.debug(colored('b:', 'blue') + '\n', b)
        return None
    return solution


class GenericObjective(torch.nn.Module):
    def __init__(self, graph, relaxed_max=None, relaxed_min=None):
        torch.nn.Module.__init__(self)
        self.graph = graph
        self.relaxed_max = relaxed_max
        self.relaxed_min = relaxed_min

        self.register_buffer('mask', graph.mask)
        self.register_buffer('times', graph.times)
        self.register_buffer('longest', graph.longest)
        self.register_buffer('costs', graph.costs)
        self.register_buffer('aug_costs', graph.aug_costs)
        self.register_buffer('target_mask', graph.target_mask)
        self.register_buffer('non_target_mask', ~graph.target_mask)
        # self.target_map = graph.aug_target_map
        self.aug_nodes_n = graph.aug_nodes_n
        self.targets_n = graph.targets_n

    def early_stop(self, val):
        return False

    def schedule(self, total_steps, current_step):
        pass

    def get_expectations(self, strategy):
        A = torch.eye(self.aug_nodes_n, device=strategy.device) - strategy * self.non_target_mask[None, ..., None]
        b = (strategy * self.times).sum(dim=-1) * self.non_target_mask[None, ...]
        return solve_system(A, b)

    def get_second_moment(self, strategy, expectations):
        A = torch.eye(self.aug_nodes_n, device=strategy.device) - strategy * self.non_target_mask[None, ..., None]
        b = (strategy * self.times * (self.times + 2 * expectations[..., None])).sum(dim=-1) * self.non_target_mask[
            None, ...]
        return solve_system(A, b)

    def get_variance(self, strategy, expectations):
        A = torch.eye(self.aug_nodes_n, device=strategy.device) - strategy * self.non_target_mask[None, ..., None]
        b = (strategy * (self.times[None, :] + expectations[..., None, :] - expectations[..., None]) ** 2).sum(dim=-1) * \
            self.non_target_mask[None, ...]
        return solve_system(A, b)

    def get_std(self, strategy, expectations):
        return torch.sqrt(self.get_variance(strategy, expectations) + 1e-14)

    def get_node_stationary(self, strategy):
        B = strategy.transpose(0, 1) - torch.eye(self.aug_nodes_n)
        A = torch.cat((B[:-1], torch.ones_like(B[0])[None, :]), 0)
        b = torch.zeros_like(strategy[0])
        b[-1] = 1
        return solve_system(A, b)

    def get_edge_stationary(self, strategy):
        node_stationary = self.get_node_stationary(strategy)
        distribution = strategy * node_stationary[:, None] * self.times
        return distribution / distribution.sum()

    def get_p_values(self, strategy, levels):
        p_values = []
        if levels:
            # tensor - for each target, strategy matrix with zero rows for the target
            x = strategy * self.non_target_mask[:, :, None]
            # tensor - for each target, strategy matrix with self-loops on the target nodes
            x = x + torch.eye(self.aug_nodes_n) * self.target_mask[:, None]
            for level in levels:
                # steal -> steps, "-1" stands for the first edge (on which the attack starts)
                powers = torch.floor(level / self.costs) - 1
                y = x.clone().detach()
                for idx, power in enumerate(powers):
                    y[idx] = torch.matrix_power(y[idx], int(power))
                # sum probs for different mem. elems of each target, select the best attack
                N = (y * self.target_mask[:, None]).max(dim=2).values
                # in N, there are probs to reach target within the bound; i.e. P(steal <= level)
                # so, we return complementary P(steal > bound) = 1 - N
                p_values.append((1 - N).max().detach().item())
        return p_values
