# Standard Imports
from abc import ABC, abstractmethod
import functools
import logging

# Third-party Imports
import torch
import numpy as np

# Local Imports
from src.cpp_extensions.evaluators import create_distribution_from_ratios
from src.model.objectives.generic_matrix_objective import GenericMatrixObjective
from src.io.logging import get_logger

logger = get_logger('objective')


class StablePatrolling(GenericMatrixObjective):
    mode: str = 'min'

    def __init__(
            self,
            goal_ratios: list[float],
            beta: float,
            gamma: float,
            global_badness: 'TensorReduction',
            penalty_1_reduction: 'TensorReduction',
            weigh_pen_1: bool = False,
            weigh_pen_2: bool = False,
            **kwargs
    ) -> None:

        assert beta >= 0
        assert gamma >= 0
        assert gamma + beta <= 1

        GenericMatrixObjective.__init__(self, **kwargs)
        self.beta = beta
        self.gamma = gamma
        # for the usage of incoming edges, irrelevant due to uniq edge length
        self.global_badness = global_badness
        self.penalty_1_reduction = penalty_1_reduction
        self.weigh_pen_1 = weigh_pen_1
        self.weigh_pen_2 = weigh_pen_2
        self.goal_distribution = create_distribution_from_ratios(goal_ratios)

    def forward(self, strategy):
        try:
            # shape: (BSCCs, AugNodes, AugNodes)
            bscc_strategies = self.get_bscc_strategies(strategy)
        except RuntimeError as e:
            logger.error(e)
            extras = dict(bscc_index=-1,
                          global_badness=None,
                          penalty_1=None,
                          penalty_2=None)
            return float('inf'), strategy.numpy(), extras

        # shape: (BSCCs, 1, AugNodes, AugNodes)
        bscc_goal_strategies = bscc_strategies[:, None, ...]
        # shape: (BSCCs, Targets, Nodes)
        vt_goal_vertex_e = self.get_expectations(bscc_goal_strategies)

        # shape: (BSCCs, Targets, Nodes)
        vt_goal_vertex_sec = self.get_second_moment(bscc_goal_strategies, vt_goal_vertex_e)

        rt_goal_vertex_e = self.get_expected_return_times(bscc_goal_strategies, vt_goal_vertex_e)

        rt_goal_vertex_sec = self.get_sec_moment_return_times(bscc_goal_strategies,
                                                              vt_goal_vertex_e,
                                                              vt_goal_vertex_sec)

        rt_goal_vetex_std = torch.sqrt(torch.abs(rt_goal_vertex_sec - rt_goal_vertex_e ** 2) + 1e-14)

        # shape: (BSCCs, AugNodes)
        inv = self.get_node_stationary(bscc_strategies)

        # shape:  (BSCCs, Goals, AugNodes)
        inv_within_goals = inv[None, ...] * self.aug_goal_mask

        # shape: (BSCCs, Goals)
        inv_on_goals = inv_within_goals.sum(dim=-1)

        # shape: (BSCCs, Goals, AugNodes)
        inv_given_goal = inv_within_goals / inv_within_goals.sum(dim=-1, keepdim=True)

        # shape: (BSCCs, Goals)
        rt_goal_e = (rt_goal_vertex_e * inv_given_goal).sum(dim=-1)
        # shape: (BSCCs, Goals)
        rt_goal_sec = (rt_goal_vertex_sec * inv_given_goal).sum(dim=-1)
        # shape: (BSCCs, Goals)
        rt_goal_std = torch.sqrt(torch.abs(rt_goal_sec - rt_goal_e ** 2) + 1e-14)

        penalty_1_weights = inv_on_goals if self.weigh_pen_1 else torch.ones_like(inv_on_goals)
        penalty_2_weights = inv_within_goals if self.weigh_pen_2 else self.aug_goal_mask

        # shape: (BSCCs)
        global_badness = self.global_badness.evaluate(inv_on_goals - self.goal_distribution)
        penalty_1 = self.penalty_1_reduction.evaluate(rt_goal_std * penalty_1_weights)
        penalty_2 = (rt_goal_vetex_std * penalty_2_weights).sum(dim=-1).sum(dim=-1)

        global_badness_sg = global_badness.detach()
        penalty_1_sg = penalty_1.detach()
        penalty_2_sg = penalty_2.detach()

        # shape: (BSCCs)
        bscc_vals = (1 - self.beta - self.gamma) * global_badness \
            + self.beta * ((global_badness_sg + 1) / (penalty_1_sg + 1)) * penalty_1 \
            + self.gamma * ((global_badness_sg + 1) / (penalty_2_sg + 1)) * penalty_2

        val_min, bscc_index = bscc_vals.min(dim=-1)

        if self.training:
            loss = self.relaxed_min(bscc_vals)
            return loss, val_min.item(), None

        # return for eval
        extras = dict(bscc_index=bscc_index.item(),
                      global_badness=global_badness[bscc_index].item(),
                      penalty_1=penalty_1[bscc_index].item(),
                      penalty_2=penalty_2[bscc_index].item())

        return val_min.item(), bscc_strategies[bscc_index].detach().numpy(), extras


class TensorReduction(ABC):
    def __init__(self) -> None:
        super().__init__()

    @staticmethod
    @abstractmethod
    def evaluate(tensor: torch.Tensor) -> torch.Tensor:
        """
        ## Args:
            tensor: torch.Tensor of shape (..., n)

        ## Returns:
            torch.Tensor of shape (...)
        """


class SumReduction(TensorReduction):
    @staticmethod
    def evaluate(tensor: torch.Tensor) -> torch.Tensor:
        return tensor.sum(dim=-1)


class L1Reduction(TensorReduction):
    @staticmethod
    def evaluate(tensor: torch.Tensor) -> torch.Tensor:
        return torch.abs(tensor).sum(dim=-1)


class L2Reduction(TensorReduction):
    @staticmethod
    def evaluate(tensor: torch.Tensor) -> torch.Tensor:
        return torch.sqrt((tensor ** 2).sum(dim=-1) + 1e-14)


class MaxReduction(TensorReduction):
    @staticmethod
    def evaluate(tensor: torch.Tensor) -> torch.Tensor:
        result: torch.Tensor = torch.abs(tensor).max(dim=-1).values
        return result
