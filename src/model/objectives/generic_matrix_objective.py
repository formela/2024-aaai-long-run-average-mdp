# Standard Imports
from typing import Dict, Tuple, List

# Third-party Imports
import torch
import numpy as np
from termcolor import colored

# Local Imports
from cpp_evaluators import CppSCCsEvaluator
from src.model.utils import solve_system, expand_graph_mask, compute_target_mask, TorchSCCsEvaluator
from src.io.logging import get_logger

logger = get_logger('objective')

dtypes = {
    'float32': torch.float32,
    'float64': torch.float64,
    'float': torch.float32,
    'double': torch.float64,
}


class GenericMatrixObjective(torch.nn.Module):
    def __init__(
            self,
            nodes_n: int,
            aug_nodes: List[Tuple[Tuple[Tuple[int, int], ...], int]],
            times: torch.Tensor,
            strategy_mask: torch.Tensor,
            num_of_agents: int,
            goal_sets: List[List[int]],
            costs: torch.Tensor,
            agent_resilience: int = 0,
            resilience_rate: float = 0.1,
            rounding_threshold: float = 0.01,
            relaxed_max=None,
            relaxed_min=None,
            device='cpu',
            data_type=torch.float64,
    ):
        torch.nn.Module.__init__(self)

        self.device = device
        if isinstance(data_type, str):
            data_type = dtypes[data_type]
        self.data_type = data_type

        self.num_of_agents: int = num_of_agents
        self.goal_sets: List[List[int]] = goal_sets
        self.target_to_goal: Dict[int, int] = {target: i for i, goal_set in enumerate(self.goal_sets)
                                               for target in goal_set}
        self.targets_n = len(self.target_to_goal)
        self.nodes_n: int = nodes_n  # number of nodes in the original graph
        self.aug_nodes = aug_nodes
        self.aug_nodes_n: int = len(aug_nodes)
        self.goals_n: int = len(goal_sets)
        self.agent_resilience: int = agent_resilience  # number of agents that can fail
        self.resilience_rate: float = resilience_rate  # coefficient of the resilient part of the objective
        self.rounding_threshold: float = rounding_threshold  # threshold for rounding the strategy to 0

        self.register_buffer('mask', strategy_mask)
        self.register_buffer('times', expand_graph_mask(times, aug_nodes))
        self.register_buffer('longest', self.times.max(dim=0).values)
        self.register_buffer('goal_costs', costs)
        goal_mask, goal_costs, goal_map, self.target_descriptions = self._init_target_mask()
        self.register_buffer('aug_goal_costs', goal_costs)
        self.register_buffer('aug_goal_mask', goal_mask)
        self.register_buffer('non_goal_mask', ~self.aug_goal_mask)
        self.register_buffer('goal_map', goal_map)

        self.relaxed_max = relaxed_max
        self.relaxed_min = relaxed_min

        # todo: move to hydra config
        self.bscc = CppSCCsEvaluator(self.aug_nodes_n)
        # self.bscc = TorchSCCsEvaluator()

    def _init_target_mask(self) -> Tuple[
        torch.Tensor,
        torch.Tensor,
        torch.Tensor,
        List[Tuple[str, float, Tuple[int, int]]]
    ]:
        """
        Initialize target mask and its descriptors (cost, goal_map, description).
        @return: aug_goal_mask, aug_goal_costs, goal_map, goal_descriptions

        aug_goal_mask: boolean tensor of shape (AG, aug_nodes_n) where AG is the number of augmented goals
        (e.g. normal goals + residual goals). Rows are partitioned into several groups (e.g. normal goals,
        residual goals, etc.).
        aug_goal_costs: tensor of shape (AG) with costs of reaching each augmented goal.
        goal_map: tensor of shape (AG) with indices of the original goals corresponding to each augmented goal.
        goal_groups: a list of tuples (label, weight, (lefgoal_mapt, right)). Each tuple describes a group of augmented goals.
        Label is used to print the group value, weight is the groups value in the objective, left and right are
        indices of the group in the augmented goal mask.
        """
        basic_goal_mask, basic_goal_map = compute_target_mask(
            self.aug_nodes, self.goal_sets, self.num_of_agents, self.nodes_n, f=0, device=self.device
        )
        masks = [basic_goal_mask]
        indices = [basic_goal_map]
        left = 0
        right = len(basic_goal_map)
        descriptions = [('', 1, (left, right))]

        if self.agent_resilience > 0:
            resilient_goal_mask, resilient_gaol_map = compute_target_mask(
                self.aug_nodes, self.goal_sets, self.num_of_agents, self.nodes_n,
                f=self.agent_resilience, device=self.device
            )
            masks.append(resilient_goal_mask)
            indices.append(resilient_gaol_map)
            left = right
            right += len(resilient_gaol_map)
            descriptions.append((f'resilient({self.agent_resilience})', self.resilience_rate, (left, right)))

        aug_goal_mask = torch.cat(masks, dim=0)
        goal_map = torch.cat(indices, dim=0)
        aug_goal_costs = self.goal_costs[goal_map]

        return aug_goal_mask, aug_goal_costs, goal_map, descriptions

    def early_stop(self, val):
        return False

    def schedule(self, total_steps, current_step):
        pass

    def get_expectations(self, strategy):
        """
        @param strategy:
        @return: tensor EX of dimension [..., Targets, AugNodes],
        where EX[i][j] is the expected time to visit i-th target from j-th node
        with respect to strategy (call it P)
        and time of edges self.times (call it T).

        Also, let n = AugNodes and m = Targets.

        P and T are of shape (n, n).

        self.non_target_mask (call it M) is a matrix of shape (m, n)
        with M[i][j] = 0 iff j's target is i, 1 otherwise.

        ## Derivation

        Fix target i and node j.
        The expected time to visit i from node j EX[i][j] is equal to 0 if i is a j's target,
        otherwise EX[i][j] = \sum_{k = 1}^{n} P[j][k] * E(X[i][j] | j -> k)
        = \sum_{k = 1}^{n} P[j][k] * E(T[j][k] + X[i][k])
        = \sum_{k = 1}^{n} P[j][k] * (T[j][k] + EX[i][k])
        = \sum_{k = 1}^{n} P[j][k] * T[j][k] + \sum_{k = 1}^{n} P[j][k] * EX[i][k]
        = sum(P[j] * T[j]) + P[j] . EX[i]
        where . is the standard vector dot product.

        For both i is/is not j's target, we can write
        EX[i][j] = (sum(P[j] * T[j]) + P[j] . EX[i]) * M[i][j]

        Hence, for a fixed target i,
        EX[i] is a vector of n numbers given as:
        EX[i] = (f(P * T) + P @ EX[i]) * M[i]
            = f(P * T) * M[i] + P @ EX[i] * M[i]
            = f(P * T) * M[i] + (P * M'[i]) @ EX[i]

        where:
        - @ is a matrix multiplication of two matrices.
        - * is an element-wise product of two matrices.
        - f is a function that sums the last dimension of the matrix.
        - M'[i] is a matrix of shape (n, n) obtained by copying M[i] n-times as a column.

        Moving EX[i] to one side we obtain:
        (I_n - P * M'[i]) @ EX[i] = f(P * T) * M[i]

        which we can solve as a linear equation of the form
        A @ X = b, for A = I_n - P * M'[i] and b = f(P * T) * M[i]
        """

        A = torch.eye(self.aug_nodes_n) - strategy * self.non_goal_mask[..., None]
        b = (strategy * self.times).sum(dim=-1) * self.non_goal_mask
        return solve_system(A, b)

    def get_expected_return_times(self, strategy, expectations):
        """
        @param strategy: shape (..., AugNodes, AugNodes)
        @param expectations: precomputed EX
        @return: tensor EY of dimension [..., Targets, AugNodes],
        where EY[i][j] is the expected return time from node i to target j
        with respect to strategy (call it P)
        and time of edges self.times (call it T).

        Also, let n = AugNodes and m = Targets.

        P and T are of shape (n, n).

        ## Derivation

        Fix target i and node j.
        The expected time to re-visit i from node j is
        EY[i][j] = \sum_{k = 1}^{n} P[j][k] * E(X[i][j] | j -> k)
        = \sum_{k = 1}^{n} P[j][k] * E(T[j][k] + X[i][k])
        = \sum_{k = 1}^{n} P[j][k] * (T[j][k] + EX[i][k])
        = \sum_{k = 1}^{n} P[j][k] * T[j][k] + \sum_{k = 1}^{n} P[j][k] * EX[i][k]
        = sum(P[j] * T[j]) + P[j] . EX[i]
        where . is the standard vector dot product.

        Hence, for a fixed target i,
        EY[i] is a vector of n numbers given as:
        EY[i] = f(P * T) + P @ EX[i]
              = f(P * T) + f(P * EX''[i])
              = f(P * (T + EX''[i]))

        where:
        - @ is a matrix multiplication of two matrices.
        - * is an element-wise product of two matrices.
        - f is a function that sums the last dimension of the matrix.
        - EX''[i] is a matrix of shape (n, n) obtained by copying vectors EX[i] n-times as a row.
          i.e. add a new last but one dimension.
        """

        return (strategy * (self.times + expectations[..., None, :])).sum(dim=-1)

    def get_second_moment(self, strategy, expectations):
        """
        @param strategy:
        @param expectations: precomputed EX
        @return: tensor EX^2 of dimension [..., Targets, AugNodes],
        where EX^2[i][j] is the second moment of time to visit i-th target from j-th node
        with respect to strategy (call it P)
        and time of edges self.times (call it T).

        Also, let n = AugNodes and m = Targets.

        P and T are of shape (n, n).

        self.non_target_mask (call it M) is a matrix of shape (m, n)
        with M[i][j] = 0 iff j's target is i, 1 otherwise.

        ## Derivation

        Fix target i and node j.
        The second moment of time to visit i from node j EX^2[i][j] is equal to 0 if i is a j's target,
        otherwise EX^2[i][j] = \sum_{k = 1}^{n} P[j][k] * E(X^2[i][j] | j -> k)
        = \sum_{k = 1}^{n} P[j][k] * E((T[j][k] + X[i][k])^2)
        = \sum_{k = 1}^{n} P[j][k] * E(T^2[j][k] + 2 * T[j][k] * X[i][k] + X^2[i][k])
        = \sum_{k = 1}^{n} P[j][k] * (T^2[j][k] + 2 * T[j][k] * E(X[i][k]) + EX^2[i][k])
        = \sum_{k = 1}^{n} P[j][k] * T[j][k] * (T[j][k] + 2 * E(X[i][k]) + \sum_{k = 1}^{n} P[j][k]* EX^2[i][k]
        = sum(P[j] * T[j] * (T[j] + 2 * EX[i])) + P[j] . EX^2[i]
        where . is the standard vector dot product.

        For both i is/is not j's target, we can write
        EX^2[i][j] = (sum(P[j] * T[j] * (T[j] + 2 * EX[i])) + P[j] . EX^2[i]) * M[i][j]

        Hence, for a fixed target i,
        EX^2[i] is a vector of n numbers given as:
        EX^2[i] = (f(P * T * (T + 2 * EX''[i])) + P @ EX^2[i]) * M[i]
            = f(P * T * (T + 2 * EX''[i])) * M[i] + P @ EX^2[i] * M[i]
            = f(P * T * (T + 2 * EX''[i])) * M[i] + (P * M'[i]) @ EX^2[i]

        where:
        - @ is a matrix multiplication of two matrices.
        - * is an element-wise product of two matrices.
        - f is a function that sums the last dimension of the matrix.
        - M'[i] is a matrix of shape (n, n) obtained by copying M[i] n-times as a column,
          i.e. add a new lowest dimension.
        - EX''[i] is a matrix of shape (n, n) obtained by copying vectors EX[i] n-times as a row.
          i.e. add a new last but one dimension.

        Moving EX^2[i] to one side we obtain:
        (I_n - P * M'[i]) @ EX^2[i] = f(P * T * (T + 2 * EX''[i])) * M[i]

        which we can solve as a linear equation of the form
        A @ X = b, for A = I_n - P * M'[i] and b = f(P * T * (T + 2 * EX''[i])) * M[i]
        """
        A = torch.eye(self.aug_nodes_n) - strategy * self.non_goal_mask[..., None]
        b = (strategy * self.times * (self.times + 2 * expectations[..., None, :])).sum(dim=-1) * self.non_goal_mask
        return solve_system(A, b)

    def get_sec_moment_return_times(self, strategy, expectations, second_moment):
        """
        @param strategy:
        @param expectations: precomputed EX
        @param second_moment: precomputed EX^2
        @return: tensor EY^2 of dimension [..., Targets, AugNodes],
        where EY^2[i][j] is the second moment of return time of i-th target from j-th node
        with respect to strategy (call it P)
        and time of edges self.times (call it T).

        Also, let n = AugNodes and m = Targets.

        P and T are of shape (n, n).

        ## Derivation

        Fix target i and node j.
        The second moment of time to visit i from node j is
        EY^2[i][j] = \sum_{k = 1}^{n} P[j][k] * E(X^2[i][j] | j -> k)
        = \sum_{k = 1}^{n} P[j][k] * E((T[j][k] + X[i][k])^2)
        = \sum_{k = 1}^{n} P[j][k] * E(T^2[j][k] + 2 * T[j][k] * X[i][k] + X^2[i][k])
        = \sum_{k = 1}^{n} P[j][k] * (T^2[j][k] + 2 * T[j][k] * E(X[i][k]) + EX^2[i][k])
        = \sum_{k = 1}^{n} P[j][k] * T[j][k] * (T[j][k] + 2 * E(X[i][k]) + \sum_{k = 1}^{n} P[j][k]* EX^2[i][k]
        = sum(P[j] * T[j] * (T[j] + 2 * EX[i])) + P[j] . EX^2[i]
        where . is the standard vector dot product.

        Hence, for a fixed target i,
        EY^2[i] is a vector of n numbers given as:
        EY^2[i] = f(P * T * (T + 2 * EX''[i])) + P @ EX^2[i]
                = f(P * T * (T + 2 * EX''[i])) + f(P * EX^2''[i])
                = f(P * (T * (T + 2 * EX''[i]) + EX^2''[i]))

        where:
        - @ is a matrix multiplication of two matrices.
        - * is an element-wise product of two matrices.
        - f is a function that sums the last dimension of the matrix.
        - EX''[i] is a matrix of shape (n, n) obtained by copying vectors EX[i] n-times as a row.
          i.e. add a new last but one dimension.
        - EX^2''[i] is a matrix of shape (n, n) obtained by copying vectors EX[i] n-times as a row.
          i.e. add a new last but one dimension.
        """
        res = (strategy * (self.times * (self.times + 2 *
                                         expectations[..., None, :]) + second_moment[..., None, :])).sum(dim=-1)
        return res

    def get_variance(self, strategy, expectations):
        """
        @param strategy:
        @param expectations: precomputed EX
        @return: tensor Var(X) of dimension [..., Targets, AugNodes],
        where Var(X)[i][j] is the second central moment (variance) of time to visit i-th target from j-th node
        with respect to strategy (call it P)
        and time of edges self.times (call it T).

        Also, let n = AugNodes and m = Targets.

        P and T are of shape (n, n).

        self.non_target_mask (call it M) is a matrix of shape (m, n)
        with M[i][j] = 0 iff j's target is i, 1 otherwise.

        ## Derivation

        Fix target i and node j.
        Note that Var(X) = E((X-EX)^2) and Var(X)=sum_{a} Prob(a) * E((X - EX)^2 | a) if EX is given and fixed.
        The second central moment of time to visit i from node j Var(X)[i][j] is equal to 0 if i is a j's target,
        otherwise Var(X)[i][j] = \sum_{k = 1}^{n} P[j][k] * E((T[j][k] + X[i][k] - EX[i][j])^2)
        = \sum_{k = 1}^{n} P[j][k] * E(T^2[j][k] + 2 * T[j][k] * X[i][k] + X^2[i][k]
                                       - 2 * T[j][k] * EX[i][j] - 2 * X[i][k] * EX[i][j] + (EX[i][j])^2)
        = \sum_{k = 1}^{n} P[j][k] * (T^2[j][k] + 2 * T[j][k] * EX[i][k] + EX^2[i][k]
                                       - 2 * T[j][k] * EX[i][j] - 2 * EX[i][k] * EX[i][j] + (EX[i][j])^2)
        = \sum_{k = 1}^{n} P[j][k] * (T^2[j][k] + 2 * T[j][k] * EX[i][k] + EX^2[i][k] - (EX[i][k])^2 + (EX[i][k])^2
                                       - 2 * T[j][k] * EX[i][j] - 2 * EX[i][k] * EX[i][j] + (EX[i][j])^2)
        = \sum_{k = 1}^{n} P[j][k] * (T^2[j][k] + 2 * T[j][k] * EX[i][k] + Var(X)[i][k] + (EX[i][k])^2
                                       - 2 * T[j][k] * EX[i][j] - 2 * EX[i][k] * EX[i][j] + (EX[i][j])^2)
        = \sum_{k = 1}^{n} P[j][k] * (T^2[j][k] + 2 * T[j][k] * EX[i][k] + (EX[i][k])^2
                                       - 2 * T[j][k] * EX[i][j] - 2 * EX[i][k] * EX[i][j]
                                       + (EX[i][j])^2)
          + \sum_{k = 1}^{n} P[j][k] * Var(X)[i][k]
        = \sum_{k = 1}^{n} P[j][k] * (T[j][k] + EX[i][k])^2
          - 2 * EX[i][j] * \sum_{k = 1}^{n} P[j][k] * (T[j][k] + EX[i][k])
          + (EX[i][j])^2) * \sum_{k = 1}^{n} P[j][k]
          + \sum_{k = 1}^{n} P[j][k] * Var(X)[i][k]
        = sum(P[j] * (T[j] + EX[i])^2) - 2 * EX[i][j] * sum(P[j] * (T[j] + EX[i])) + (EX[i][j])^2) + P[j] . Var(X)[i]
        where . is the standard vector dot product.

        For both i is/is not j's target, we can write
        Var(X)[i][j] = M[i][j] * (sum(P[j] * (T[j] + EX''[i])^2)
                                  - 2 * EX[i][j] * sum(P[j] * (T[j] + EX''[i]))
                                  + (EX[i][j])^2) + P[j] . Var(X)[i])

        Hence, for a fixed target i,
        Var(X)[i] is a vector of n numbers given as:
        Var(X)[i] = M[i] * (f(P * (T + EX''[i]) ** 2)
                            - 2 * EX[i] * f(P * (T + EX''[i]))
                            + EX[i] ** 2 + P @ Var(X)[i])
                  = M[i] * (f(P * (T + EX''[i]) ** 2)
                            - 2 * EX[i] * f(P * (T + EX''[i]))
                            + EX[i] ** 2)
                    + (P * M'[i]) @ Var(X)[i]
        where:
        - @ is a matrix multiplication of two matrices.
        - * is an element-wise product of two matrices.
        - f is a function that sums the last dimension of the matrix.
        - M'[i] is a matrix of shape (n, n) obtained by copying M[i] n-times as a column,
          i.e. add a new lowest dimension.
        - EX''[i] is a matrix of shape (n, n) obtained by copying vectors EX[i] n-times as a row.
          i.e. add a new last but one dimension.

        Moving Var(X)[i] to one side we obtain:
        (I_n - P * M'[i]) @ Var(X)[i] = M[i] * (f(P * (T + EX''[i]) ** 2)
                                                - 2 * EX[i] * f(P * (T + EX''[i]))
                                                + EX[i] ** 2)

        which we can solve as a linear equation of the form
        A @ X = b,
        for A = I_n - P * M'[i] and
        b = (f(P * (T + EX''[i]) ** 2) - 2 * EX[i] * f(P * (T + EX''[i])) + EX[i] ** 2) * M[i]
        """
        A = torch.eye(self.aug_nodes_n) - strategy * self.non_goal_mask[..., None]
        c = self.times + expectations[..., None, :]
        b = (strategy * (c ** 2)).sum(dim=-1) \
            - 2 * expectations * (strategy * c).sum(dim=-1) \
            + expectations ** 2
        b *= self.non_goal_mask
        return solve_system(A, b)

    def old_var(self, strategy, expectations):
        """
        An equivalent version to the previous one:
        b = (f(P * (T + EX''[i]) ** 2) - 2 * EX[i] * f(P * (T + EX''[i])) + EX[i] ** 2) * M[i]
          = (f(P * (T + EX''[i]) ** 2) - f(2 * EX'[i] * P * (T + EX''[i])) + f(P * EX'[i] ** 2)) * M[i]
          = (f(P * (T + EX''[i]) ** 2 - 2 * EX'[i] * P * (T + EX''[i]) + P * EX'[i] ** 2) * M[i]
          = (f(P * ((T + EX''[i]) ** 2 - 2 * EX'[i] * (T + EX''[i]) + EX'[i] ** 2)) * M[i]
          = f(P * (T + EX''[i] - EX'[i]) ** 2) * M[i]

        where EX[i] ** 2  = f(P * EX'[i] ** 2) because
            EX[i] ** 2 = f(P) * EX[i] ** 2, since summing rows of P gives 1.
        """
        A = torch.eye(self.aug_nodes_n) - strategy * self.non_target_mask[..., None]
        b = (strategy * (self.times + expectations[..., None, :] -
                         expectations[..., None]) ** 2).sum(dim=-1) * self.non_goal_mask
        return solve_system(A, b)

    def get_std(self, strategy, expectations):
        return torch.sqrt(self.get_variance(strategy, expectations) + 1e-14)

    def get_node_stationary(self, strategy):
        """
        @param strategy: tenzor of size [..., AugNodes, AugNodes]
        @return: tenzor of size [..., AugNodes]
        stationary distributions for the strategies of 'strategy'
        """

        A = strategy.transpose(-1, -2) - torch.eye(self.aug_nodes_n)[None, ...]
        indices = A.sum(dim=-1).argmax(dim=-1)
        b = torch.zeros(A.shape[:-1], device=strategy.device, dtype=strategy.dtype)
        for a, i in enumerate(indices):
            A[a, i] = 1
            b[a, i] = 1
        return solve_system(A, b)

    def get_edge_stationary(self, strategy):
        node_stationary = self.get_node_stationary(strategy)
        distribution = strategy * node_stationary[..., None] * self.times
        return distribution / distribution.sum()

    def get_p_values(self, strategy, levels):
        p_values = []
        if levels:
            # tensor - for each target, strategy matrix with zero rows for the target
            x = strategy * self.non_goal_mask[:, :, None]
            # tensor - for each target, strategy matrix with self-loops on the target nodes
            x = x + torch.eye(self.aug_nodes_n) * self.aug_goal_mask[:, None]
            for level in levels:
                # steal -> steps, "-1" stands for the first edge (on which the attack starts)
                powers = torch.floor(level / self.costs) - 1
                y = x.clone().detach()
                for idx, power in enumerate(powers):
                    y[idx] = torch.matrix_power(y[idx], int(power))
                # sum probs for different mem. elems of each target, select the best attack
                N = (y * self.target_mask[:, None]).max(dim=2).values
                # in N, there are probs to reach target within the bound; i.e. P(steal <= level)
                # so, we return complementary P(steal > bound) = 1 - N
                p_values.append((1 - N).max().detach().item())
        return p_values

    def get_bscc_strategies(self, strategy):
        """
            Args:
                strategy: tensor of size (AugNodes, AugNodes)
            Returns:
                tensor of size (BSCCs, AugNodes, AugNodes)
            Raises:
                RuntimeError: if all BSCCs are omitting some of the targets
        """
        with torch.no_grad():
            # shape: (BSCCs, AugNodes)
            bscc_masks = self.bscc.get_bottom_sccs(strategy)

            # erase all BSCCs that do not visit all targets
            num_of_visits = torch.sum(bscc_masks[:, None, :] * self.aug_goal_mask[None, :, :], dim=-1)
            to_be_erased = torch.any(num_of_visits == 0, dim=-1)  # shape: (BSCCs)
            bscc_masks = bscc_masks[~to_be_erased]

        if bscc_masks.shape[0] == 0:
            raise RuntimeError('All BSCCs are omitting some of the targets!')

        masked_strategies = bscc_masks[:, None, :] * bscc_masks[:, :, None] * strategy
        return masked_strategies
