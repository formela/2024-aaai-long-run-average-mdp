# Standard Imports
from abc import abstractmethod, ABC
import functools

# Third-party Imports
import torch

# Local Imports
from src.graph import Graph
from src.model.utils import augment_nodes_tuples, augment_nodes_sets
from src.io.logging import get_logger

logger = get_logger('model')


class MatrixModel(ABC, torch.nn.Module):
    def __init__(self,
                 graph: Graph,
                 strategy: functools.partial,
                 objective,
                 num_of_agents=1,
                 global_memory=1,
                 symmetric_states=False, delay_params=None, checkpoint=None):
        super().__init__()

        # delay_params = delay_params or {}

        self.graph = graph
        self.symmetric_states = symmetric_states
        self.num_of_agents = num_of_agents
        self.global_memory = global_memory

        if self.symmetric_states:
            self.aug_nodes = augment_nodes_sets(graph.nodes_n, num_of_agents, graph.memory, global_memory)
        else:
            self.aug_nodes = augment_nodes_tuples(graph.nodes_n, num_of_agents, graph.memory, global_memory)

        self.prepare_strategy(strategy)

        # TODO
        kwargs = {
            'nodes_n': self.graph.nodes_n,
            'aug_nodes': self.aug_nodes,
            'times': self.graph.times,
            'strategy_mask': self.strategy.get_full_mask(),
            'num_of_agents': num_of_agents,
            'data_type': torch.float64,
        }
        if 'costs' not in objective.keywords:
            kwargs['costs'] = graph.costs
        if 'goal_sets' not in objective.keywords:
            kwargs['goal_sets'] = graph.goal_sets

        self.add_module('objective', objective(**kwargs))

        if checkpoint:
            self.load_state_dict(state_dict=torch.load(checkpoint))

    @abstractmethod
    def prepare_strategy(self, strategy: functools.partial):
        pass

    def schedule(self, *args, **kwargs):
        self.strategy.schedule(*args, *kwargs)
        # TODO: add delay
        # if self.delay is not None:
        #     self.delay.schedule(*args, *kwargs)
        self.objective.schedule(*args, *kwargs)

    def forward(self):
        strategy = self.strategy()
        # TODO: add delay
        # if self.delay is not None:
        #     delay = self.delay()
        #     return self.objective(strategy, delay)
        # else:
        return self.objective(strategy)

    def print_strategy(self, style='label', precision=2):
        return self.strategy.print(logger, style=style, times=self.objective.times, precision=precision)
