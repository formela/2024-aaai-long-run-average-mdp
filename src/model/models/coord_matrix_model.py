# Standard Imports
import functools

# Third-party Imports
from typing_extensions import override

# Local Imports
from src.model.models.base_matrix_model import MatrixModel
from src.model.strategy import MatrixStrategy
from src.io.logging import get_logger

logger = get_logger('model')


class CoordinatedMatrixModel(MatrixModel):
    strategy: MatrixStrategy

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    @override
    def prepare_strategy(self, strategy: functools.partial):
        strategy = strategy(
            nodes_n=self.graph.nodes_n,
            graph_matrix=self.graph.mask,
            aug_nodes=self.aug_nodes,
            num_of_agents=self.num_of_agents
        )
        self.add_module('strategy', strategy)
