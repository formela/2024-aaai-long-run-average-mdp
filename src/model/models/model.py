from itertools import product, combinations_with_replacement

import torch
from src.model.strategy import MatrixStrategy, Delay, strategy_loaders
from src.model.objectives.generic_matrix_objective import GenericMatrixObjective
from src.model.utils import augment_nodes_tuples, augment_nodes_sets

from src.io.logging import get_logger
logger = get_logger('model')


class Model(torch.nn.Module):
    def __init__(self, graph, strategy_params, objective, num_of_agents=1, global_memory=1, agent_resilience=0,
                 symmetric_states=False, objective_params=None, delay_params=None, checkpoint=None):
        super().__init__()
        objective_params = objective_params or {}
        delay_params = delay_params or {}

        self.graph = graph
        self.symmetric_states = symmetric_states

        objective_class = objectives_dict[objective]

        if self.symmetric_states:
            self.aug_nodes = augment_nodes_sets(graph.nodes_n, num_of_agents, graph.memory, global_memory)
        else:
            self.aug_nodes = augment_nodes_tuples(graph.nodes_n, num_of_agents, graph.memory, global_memory)

        if 'costs' not in objective_params:
            objective_params['costs'] = graph.costs
        if 'goal_sets' not in objective_params:
            objective_params['goal_sets'] = graph.goal_sets

        if issubclass(objective_class, GenericMatrixObjective):
            self.prepare_matrix_model(
                strategy_params, objective_params, objective_class, num_of_agents, global_memory
            )
        else:
            raise NotImplementedError("Only matrix objectives are supported at the moment")

        if checkpoint:
            self.load_state_dict(state_dict=torch.load(checkpoint))

    def prepare_matrix_model(
            self, strategy_params, objective_params, objective_class, num_of_agents, global_memory
    ):
        if strategy_params.get('independent_agents', False):
            assert global_memory == 1
            agent_aug_nodes = augment_nodes_tuples(self.graph.nodes_n, 1, self.graph.memory, global_memory)
            self.add_module('strategy', MatrixStrategy(
                self.graph.nodes_n, self.graph.mask, agent_aug_nodes, num_of_agents, **strategy_params
            ))
        else:
            self.add_module('strategy', MatrixStrategy(
                self.graph.nodes_n, self.graph.mask, self.aug_nodes, num_of_agents, **strategy_params
            ))

        self.add_module('objective', objective_class(
            self.graph.nodes_n, self.aug_nodes,
            self.graph.times, self.strategy.get_full_mask(),
            num_of_agents,
            data_type=torch.float64, **objective_params
        ))

    def schedule(self, *args, **kwargs):
        self.strategy.schedule(*args, *kwargs)
        # TODO: add delay
        # if self.delay is not None:
        #     self.delay.schedule(*args, *kwargs)
        self.objective.schedule(*args, *kwargs)

    def forward(self):
        strategy = self.strategy()
        # TODO: add delay
        # if self.delay is not None:
        #     delay = self.delay()
        #     return self.objective(strategy, delay)
        # else:
        return self.objective(strategy)

    def print_strategy(self, style='label', precision=2):
        return self.strategy.print(logger, style=style, times=self.objective.times, precision=precision)
