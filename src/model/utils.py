# Standard Imports
from typing import Union, Tuple
from itertools import product, combinations_with_replacement, combinations

# Third-party Imports
import torch
import numpy as np
from termcolor import colored

# Local Imports
from src.io.logging import get_logger

logger = get_logger('model')


def expand_graph_mask(adjacency_mask: torch.Tensor, aug_nodes):
    """
    @param adjacency_mask: binary adjacency matrix of the original graph
    @param aug_nodes: list of augmented nodes of a form [(((n1, m1), (n2, m2), ...), gm1), ...]
    @return: adjacency mask of the augmented graph
    """
    positions = torch.tensor(
        [[n for n, m in an] for an, _ in aug_nodes],
        dtype=torch.long, device=adjacency_mask.device)  # (num_of_aug_nodes, num_of_agents)

    return torch.gather(
        adjacency_mask[positions], 2,
        positions.t().unsqueeze(0).expand(len(aug_nodes), -1, -1)
    ).prod(dim=1)


def compute_target_mask(
        aug_nodes: list[tuple[tuple[tuple[int, int], ...], int]],
        goal_sets: list[list[int]],
        num_of_agents: int,
        nodes_n: int,
        f: int = 0,
        device: Union[str, torch.device] = 'cpu'
) -> tuple[torch.Tensor, torch.Tensor]:
    """
    @param aug_nodes: list of augmented nodes of a form [(((n1, m1), (n2, m2), ...), gm1), ...]
    @param goal_sets: list of goal sets of a form [[n1, n2, ...], [n3, n4, ...], ...]
    @param num_of_agents: number of agents
    @param nodes_n: number of nodes in the original graph
    @param f: number of malfunctioning agents
    @param device: device to use
    @return: goal_mask, goal_map

    goal_mask: binary mask of the goal nodes of shape (len(goal_sets) * (num_of_agents choose f), len(aug_nodes))
    goal_map: tensor of shape (len(target_mask)) containing the mapping from goal_mask to original goal_sets
    """
    positions = torch.tensor(
        [[n for n, m in an] for an, _ in aug_nodes],
        dtype=torch.long, device=device
    ).t()  # (num_of_agents, num_of_aug_nodes)

    goal_positions = torch.tensor(
        sum([[(i, n) for n in goal] for i, goal in enumerate(goal_sets)], []),
        dtype=torch.long, device=device
    )
    goal_mask = torch.zeros((len(goal_sets), nodes_n), dtype=torch.bool, device=device)  # (goal_sets, nodes_n)
    goal_mask[goal_positions[:, 0], goal_positions[:, 1]] = 1

    agent_combinations = list(combinations(range(num_of_agents), num_of_agents - f))
    target_mask = torch.zeros((len(agent_combinations), len(goal_sets), len(aug_nodes)),
                              dtype=torch.bool, device=device)

    for i, a in enumerate(agent_combinations):
        agent_indices = torch.tensor(a, dtype=torch.long, device=device)
        a_positions = positions[agent_indices]\
            .unsqueeze(0).expand(len(goal_sets), -1, -1)  # (goal_sets, num_of_agents - kappa, aug_nodes)
        src = goal_mask[..., None].expand(-1, -1, len(aug_nodes))  # (goal_sets, nodes_n, aug_nodes)
        target_mask[i] = torch.gather(src, 1, a_positions).max(dim=1).values

    return target_mask.reshape(-1, len(aug_nodes)), \
        torch.arange(0, len(goal_sets), dtype=torch.long, device=device).repeat(len(agent_combinations)).flatten()


def augment_nodes_tuples(n: int, num_of_agents: int, memory_elements: list[int], global_memory: int) -> list[
    tuple[tuple[tuple[int, int], ...], int]
]:
    """
    Produces the list of all possible augmented nodes of a form [(((n1, m1), (n2, m2), ...), gm1), ...].

    The order of agents in the tuple is taken into account.

    @param n: Number of nodes in the original graph
    @param num_of_agents: Number of agents
    @param memory_elements: Number of memory elements for each node
    @param global_memory: Global memory size
    @return: A list of all possible augmented nodes
    """
    nodes = sum([[(node, m) for m in range(memory_elements[node])] for node in range(n)], [])
    nodes = list(product(nodes, repeat=num_of_agents))
    nodes = list(product(nodes, range(global_memory)))
    return nodes


def augment_nodes_sets(n: int, num_of_agents: int, memory_elements: list[int], global_memory: int) -> list[
    tuple[tuple[tuple[int, int], ...], int]
]:
    """
    Produces the list of all possible augmented nodes of a form [(((n1, m1), (n2, m2), ...), gm1), ...].

    The order of agents in the tuple is not taken into account. I.e. ni <= nj for all i <= j inside the tuple.

    @param n: Number of nodes in the original graph
    @param num_of_agents: Number of agents
    @param memory_elements: Number of memory elements for each node
    @param global_memory: Global memory size
    @return: A list of all possible augmented nodes
    """
    nodes = sum([[(node, m) for m in range(memory_elements[node])] for node in range(n)], [])
    indices = list(combinations_with_replacement(range(len(nodes)), num_of_agents))
    nodes = [tuple(nodes[n] for n in t) for t in indices]
    nodes = list(product(nodes, range(global_memory)))
    return nodes


def aug_nodes2strings(
        aug_nodes: list[tuple[tuple[tuple[int, int], ...], int]],
        node_names: list,
) -> list[str]:
    """
    Converts a list of augmented nodes of a form [(((n1, m1), (n2, m2), ...), gm1), ...]
    to a list of augmented node labels.

    @param aug_nodes: A list of augmented nodes
    @param node_names: A list of node names
    @return: A list of strings
    """
    return [f'[{",".join([f"v{node_names[n]}m{m}" for n, m in an])}]gm{gm}' for an, gm in aug_nodes]


def solve_system(A: torch.Tensor, b: torch.Tensor):
    """
    Solve several systems of linear equations. The first dimension of A and b is the batch dimension.

    @param A: A tensor of shape (batch_size, n, n)
    @param b: A tensor of shape (batch_size, n)
    @return: A tensor of shape (batch_size, n) containing the solution of the system of linear equations
    """
    try:
        solution = torch.linalg.solve(A, b)
        return solution
    except RuntimeError as error_msg:
        logger.error('torch.linalg.solve failed with runtime error: %s', error_msg)
        logger.debug(colored('A:', 'blue') + '\n', A)
        logger.debug(colored('b:', 'blue') + '\n', b)
        return None


class TorchSCCsEvaluator:
    def _identify_components(self, strategy: torch.Tensor) -> Tuple[torch.Tensor, torch.Tensor, torch.Tensor]:
        M = strategy.gt(0).long()
        M.fill_diagonal_(1)

        # transitive closure of M
        for i in range(int(np.ceil(np.log2(M.shape[0]))) + 1):
            M = M @ M
            M.clamp_(0, 1)

        # remove asymmetric edges
        S = M * M.t()

        # compute scc index for each node
        labels = torch.arange(1, S.shape[0] + 1, device=M.device, dtype=torch.long)
        scc_labels = (S * labels[:, None]).max(dim=0).values

        return M, S, scc_labels

    def get_all_sccs(self, strategy: torch.Tensor) -> torch.Tensor:
        with torch.no_grad():
            M, S, scc_labels = self._identify_components(strategy)
            # select rows representing scc
            scc_indices = torch.unique(scc_labels) - 1
            return S[scc_indices]

    def get_bottom_sccs(self, strategy: torch.Tensor) -> torch.Tensor:
        with torch.no_grad():
            M, S, scc_indices = self._identify_components(strategy)

            # filter vertices that can reach a different scc
            in_bscc = torch.logical_or(M * scc_indices[None, :] == scc_indices[:, None], M == 0).all(dim=1)
            # select rows representing bscc
            bscc_indices = torch.unique(scc_indices[in_bscc]) - 1
            return S[bscc_indices]
