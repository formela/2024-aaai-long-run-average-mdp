from src.graph import get_graph
from src.model import Model

from src.io.logging import get_logger
logger = get_logger('eval')


def main(seed, reporting_params, epochs, graph_params, model_params, trainer_params):
    graph = get_graph(**graph_params)
    model = Model(graph, **model_params)

    model.eval()
    val, strat, _ = model()
    logger.info('The value is: %s', val)
    model.print_strategy()
