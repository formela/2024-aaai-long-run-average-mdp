# Standard Imports
import functools

# Third-party Imports
import ray

# Local Imports
from src.io.reporting import ExperimentReporting
from src.launch import AbstractTrain, Run
from src.graph import Graph


def main(cfg):
    mode = objectives_dict[cfg['model_params']['objective']].mode
    report = hydra.utils.instantiate(
        cfg['reporting']['experiment_reporter'], mode, OmegaConf.to_container(cfg, resolve=False)
    )

    cfg = OmegaConf.to_container(cfg, resolve=True)

    graph = get_graph(**cfg['graph_params'])
    report.add_graph(graph)
    experiment_id = report.get_id()

    ray_params = cfg.get('ray', {})
    ray_params.setdefault('logging_level', 0)
    # ray_params.setdefault('address', 'auto')
    ray.init(**ray_params)

    train_remote = ray.remote(train_one_epoch)
    kwargs = dict(
        graph=graph,
        mode=mode,
        experiment_id=experiment_id,
        cfg=cfg,
    )
    kwargs_id = {key: ray.put(arg) for key, arg in kwargs.items()}
    futures = [train_remote.remote(epoch=epoch, reconfigure=True, **kwargs_id) for epoch in range(cfg['epochs'])]

    while futures:
        finished, futures = ray.wait(futures, num_returns=1)
        metrics = ray.get(finished[0])
        report.update(metrics)

    report.dump()
    ray.shutdown()
