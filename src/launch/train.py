# Standard Imports
from typing import Union, Optional
import functools
from abc import ABC

import hydra.utils
# Third-party Imports
import ray

# Local Imports
from src.__main__ import replace_key_recursive
from src.io.reporting import ExperimentReporting, RunReporting
from src.launch import AbstractTask
from src.model.models.base_matrix_model import MatrixModel as Model
from src.graph import Graph
from src.trainer import StrategyTrainer
from src.utils import set_seed


class Run:
    report: RunReporting
    model: Model
    trainer: StrategyTrainer
    seed: int
    graph: Graph
    epoch: int

    def __init__(
            self,
            reporter: functools.partial,
            model: functools.partial,
            trainer: functools.partial,
            seed: int,
            graph: Graph,
            epoch: int,
            experiment_id: str
    ) -> None:
        seed = set_seed(seed, epoch)
        self.report = reporter(epoch=epoch, experiment_id=experiment_id, seed=seed)
        self.model = model(graph=graph)
        self.trainer = trainer(model=self.model, report=self.report)

    def execute(self):
        self.trainer.train()
        return self.report.dump()


class AbstractTrain(AbstractTask, ABC):
    report: ExperimentReporting
    graph: Graph
    run: Union[functools.partial, dict]
    epochs: int

    def __init__(
            self,
            reporter: ExperimentReporting,
            graph: Graph,
            run: functools.partial,
            epochs: int
    ) -> None:
        self.report = reporter
        self.graph = graph
        self.run = run
        self.epochs = epochs
        self.__report_inputs()

    def __report_inputs(self):
        self.report.add_graph(self.graph)
        # save config to report


class Train(AbstractTrain):
    report: ExperimentReporting
    graph: Graph
    run: functools.partial
    epochs: int

    def execute(self):
        experiment_id = self.report.experiment_id
        for epoch in range(self.epochs):
            run = self.run(graph=self.graph,
                           epoch=epoch,
                           experiment_id=experiment_id)
            metrics = run.execute()
            self.report.update(metrics)

        self.report.dump()


@ray.remote
def execute_run(run_cfg: dict, epoch: int, graph: Graph, experiment_id: str):
    run = hydra.utils.instantiate(
        run_cfg,
        _recursive_=True,
        _convert_='all',
        epoch=epoch,
        graph=graph,
        experiment_id=experiment_id)
    return run.execute()


class TrainParallel(AbstractTrain):
    report: ExperimentReporting
    graph: Graph
    run: dict
    epochs: int
    ray_kwargs: dict

    def __init__(
            self,
            reporter: ExperimentReporting,
            graph: Graph,
            run: functools.partial,
            epochs: int,
            ray_kwargs: Optional[dict] = None
    ) -> None:
        super().__init__(reporter, graph, run, epochs)
        self.ray_kwargs = ray_kwargs or {}

    @staticmethod
    def __ray_put(**kwargs) -> dict:
        return {key: ray.put(arg) for key, arg in kwargs.items()}

    def execute(self):
        ray_shutdown = False
        if not ray.is_initialized():
            ray_shutdown = True
            ray.init(**self.ray_kwargs)

        run_cfg = replace_key_recursive(self.run, 'class', '_target_')

        kwargs = self.__ray_put(graph=self.graph,
                                experiment_id=self.report.experiment_id,
                                run_cfg=run_cfg)

        futures = [execute_run.remote(epoch=epoch, **kwargs) for epoch in range(self.epochs)]

        while futures:
            finished, futures = ray.wait(futures, num_returns=1)
            metrics = ray.get(finished[0])
            self.report.update(metrics)

        self.report.dump()

        if ray_shutdown:
            ray.shutdown()
