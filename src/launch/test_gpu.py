import torch
from src.io.logging import get_logger
logger = get_logger('test_gpu')


def main(*args, **kwargs):
    logger.info('Testing GPU availability')
    devices = [d for d in range(torch.cuda.device_count())]
    if len(devices) == 0:
        logger.info('No GPU available')
    else:
        logger.info('Available GPUs:')
        for d in devices:
            logger.info(f'\t{d}: {torch.cuda.get_device_name(d)}')
