from math import ceil
import sys


def get_path_text(n: int) -> str:
    text = """# @package _global_
defaults:
  - /reporting:
      - disk
  - /logging/handlers:
      - drive
      - stream
  - /task: train
  - /graph/topology: hardcoded
  - /model: coordinated_matrix
  - /model/strategy: matrix
  - /model/objective: stable_patrolling
  - /trainer/optimizer: adam
  - /trainer/callbacks:
      - gradient_noise_basic_polynomial
      - local_badness
      # - printing

logging:
  handlers:
    stream:
      level: logging.INFO

metadata:
  project: stable_patrolling
  tag:
    - path_n
  description: |
    # Scaling experiment on path

epochs: 20

trainer:
  eval_every: 1
  steps: 800
  optimizer:
    lr: 0.05
  callbacks:
    local_badness:
      evaluator:
        goal_ratios: ${model.objective.goal_ratios}
        d: 3
        metric: cpp_evaluators.CppStabilityEvaluator.MetricTag.L2

model:
  strategy:
    rounding_threshold: 0.01
  objective:
    global_badness:
      _target_: src.model.objectives.stable_patrolling.L2Reduction
    penalty_1_reduction:
      _target_: src.model.objectives.stable_patrolling.SumReduction
    weigh_pen_1: true
    weigh_pen_2: true
"""
    text += f'    goal_ratios: {list(range(1, n + 1, 1))}\n'
    text += """

graph:
  goals:
"""
    for node in range(n):
        text += f'    - nodes: [{chr(65 + node)}]\n'
    text += """
  topology:
    settings:
      directed: true
      node_att:
        memory: 1
        value: 0
      edge_att:
        len: 1
"""
    m = ceil(n / 2)
    text += "    nodes:\n"
    for node in range(n):
        text += f'      {chr(65 + node)}:\n'
        text += f'        memory: {min(node + 1, m)}\n'
    text += "    edges:\n"

    for i in range(n):
        text += f'      - nodes: [{chr(65 + i)}, {chr(65 + i)}]\n'
        text += f'      - nodes: [{chr(65 + i)}, {chr(65 + (i + 1) % n)}]\n'

    return text


if __name__ == '__main__':
    for n in range(2, int(sys.argv[1]) + 1):
        text = get_path_text(n)

        with open(f'path_{n}.yaml', 'w') as file:
            n = file.write(text)
