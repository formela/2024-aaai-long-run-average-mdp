FROM pytorch/pytorch:1.13.1-cuda11.6-cudnn8-devel

# install dependencies
RUN pip install pipenv

COPY ./Pipfile /workspace/
RUN sed -i '/torch/d' /workspace/Pipfile
RUN sed -i '/python_version/d' /workspace/Pipfile

RUN cd /workspace && pipenv lock

RUN cd /workspace && pipenv install --system --ignore-pipfile

# copy the contents of your repo in
COPY src /workspace/src

# build cpp extensions

RUN apt update
RUN apt install -y git

COPY experiments /workspace/experiments
COPY Makefile /workspace/Makefile

RUN cd /workspace && make build_cpp