from graph_utils import Graph
from reporting import ExperimentReporting, get_git_commit_hash
from model import objectives_dict
from tune_utils import tune_me
from train import train_one_epoch


def get_commit(params):
    params['reporting_params']['commit'] = get_git_commit_hash()


@tune_me(pre_tune_hooks=[get_commit])
def main(epochs, graph_params, model_params, trainer_params, reporting_params):
    mode = objectives_dict[model_params['objective']].mode
    report = ExperimentReporting(**reporting_params, settings=locals(), mode=mode)

    graph = Graph(**graph_params)
    report.add_graph(graph)

    for epoch in range(epochs):
        metrics = train_one_epoch(epoch=epoch,
                                  seed=None,
                                  graph=graph,
                                  model_params=model_params,
                                  trainer_params=trainer_params,
                                  reporting_params=report.params,
                                  verbose=False)
        report.update(metrics)

    return report.dump()


if __name__ == '__main__':
    main()
