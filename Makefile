unit_tests:
	python3 -m pytest -m "not integration"

integration_tests:
	python3 -m pytest -m "integration"

test_all: unit_tests integration_tests

build_docker:
	docker build . -t src/engine

build_cpp:
	cd src/cpp_extensions && python setup.py build install && rm -r build cpp_evaluators.egg-info dist


run_docker:
	{ \
		docker run --rm -it \
		--name src_gpu \
		--workdir /workspace \
		--volume /var/data/$(USER)/src-devel/src:/workspace/src \
		--volume /var/data/$(USER)/src-devel/experiments:/workspace/experiments \
		--volume /var/data/$(USER)/src-devel/results:/workspace/results \
		--volume /var/data/$(USER)/src-devel/log:/workspace/log \
		--user $(shell id -u):$(shell id -g) \
		src/engine bash; \
	}

run_docker_gpu:
	{ \
		docker run --rm -it \
		--gpus all \
		--name src_gpu \
		--workdir /workspace \
		--volume /var/data/$(USER)/src-devel/src:/workspace/src \
		--volume /var/data/$(USER)/src-devel/experiments:/workspace/experiments \
		--volume /var/data/$(USER)/src-devel/results:/workspace/results \
		--volume /var/data/$(USER)/src-devel/log:/workspace/log \
		--user $(shell id -u):$(shell id -g) \
		src/engine bash; \
	}

# Sync local files to remote server
sync:
	{ \
	watch \
		"rsync -avhu --exclude-from=.rsync_ignore . /var/data/$(USER)/src-devel && \
		rsync -avhu --exclude-from=.rsync_ignore /var/data/$(USER)/src-devel/results . && \
		rsync -avhu --exclude-from=.rsync_ignore /var/data/$(USER)/src-devel/log ."; \
	}
