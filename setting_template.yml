seed: <any>  # mandatory parameter, no default value
# description: the seed for (pseudo)random choices
# values: null - random; a non-null value fixes seeds of randomness for reproducibility issues


verbosity: 0-3  # mandatory parameter, no default value
# description: verbosity level
# values: 0=silent, 1=prints val per epoch, 2=prints val per step, 3=prints all (debug mode)

ray_params: # optional, dictionary is passed to ray.init
  address: auto

reporting_params:
  tag: <str>  # optional
  database: <str>  # mandatory; one of 'disk' or 'mongo'
  database_params:
    # params for 'disk':
    results_dir: <local path to a directory> # mandatory; path of a directory where the results are stored; e.g., `results/experiment_name/{timestamp}`
    plot: <bool>  # mandatory; enables plotting of the runtimes and strategy values during the optimization
    save_checkpoints: <bool>  # mandatory; enables export of all torch parameter values
    save_strategies: <bool>  # mandatory; enables export of optimized strategies as numpy matrices
    # params for 'mongo':
    client_params: # mandatory; params passed to pymongo.MongoClient
      host: <str> # hostname or ip address
      # other params necessary to access MongoDB
      # username: <str>
      # password: <str>
      # ...
    db_name: <str> # mandatory;
    sync_after: <int> # save events to db by batches


graph_params:
  loader: <graph generator name>  # mandatory; values: `identity` or name of a graph generator from graph_collections.py; see graphs_dict at the end of graph_collections.py
  loader_params:
    <loader parameter1>: <value1>  # highly dependent on the chosen loader; see the corresponding procedure in graph_collections.py
    <loader parameter2>: <value2>
  modifiers:  # optional; list of graph modifiers
    <modifier_name>:  # values: see the graph-modifier names in graph_modifier_from_string at the end of graph_modifiers.py
       <modifier parameter1>: <value1>  # see the chosen modifier in graph_modifiers.py
       <modifier parameter2>: <value2>


epochs: <int>  # mandatory parameter, no default value
# description: number of optimization runs starting from random initialization
# values: any integer


trainer_params:
  steps: <int>  # mandatory parameter, no default value
  eval_every: <int>  # optional, default: 1;
  optimizer: <optimizer name>  # mandatory; see values in optimizer_from_string in trainer.py
  optimizer_params:  # optional, depends on the chosen optimizer
    <optimizer parameter1>: <value1>
    <optimizer parameter2>: <value2>
  noise: <noise name>  # optional, default: null; see values in noise_from_string in trainer.py
  noise_params:  # optional, depends on the chosen noise
    <noise parameter1>: <value1>
    <noise parameter2>: <value2>
  scheduler: <scheduler name>  # optional, default: null; see values in scheduler_from_string in trainer.py
  scheduler_params:  # optional, depends on the chosen scheduler
    <scheduler parameter1>: <value1>
    <scheduler parameter2>: <value2>

model_params:
  objective: <element of objective_dict>  # mandatory
  # description: the game objective
  # values: see keys of objectives_dict specified at the end of model.py
  objective_params:  # optional, depends on the chosen objective;
  # e.g. relaxed_max: <element of loss_dict>, relaxed_max_params.ord: 1-3, relaxed_max_params.eps: <float>, beta <float>

  strategy_params:
    rounding_threshold: <float>  # mandatory, no default value
    # description: probabilities lower than this value are rounded to 0 during non-training forward evaluation
    loader:  # optional, default null;
      # description: one of 'file', 'mongodb', 'array'
    loader_params:
      # params for 'file':
      path: <path to strategy.npy>
      # params for 'mongodb':
      host: <str>
      db: <str>
      username: <str>
      password: <str>
      run_id: <run_id>
      # params for 'array'
      array: <array of arrays of probabilities>

  delay_params:
    rounding_threshold: <float>  # default: 0; it is recommended to use (possibly negative) powers of 2
    # description: on evaluation, all delays are rounded down to integer multiples of rounding_threshold
    #              zero value has no effect
    training_start: <float> # between 0 and 1, default 0;
    # description: delays are trained only when step > training_start * trainer_params.steps
    lr: <float> # default null
    # description: if set, delay is trained with given (possibly different) learning rate

  checkpoint: <path to checkpoint.pth>  # optional, default null;
  # description: the training is initialized in the configuration stored in the specified checkpoint
  #              this is more precise than loading a strategy
