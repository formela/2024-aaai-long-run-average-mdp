import ray
from ray import tune
from ray.tune.search import BasicVariantGenerator
from ray.tune.search.bayesopt import BayesOptSearch
from utils import parse_params
import warnings
from tables import NaturalNameWarning

warnings.filterwarnings('ignore', category=NaturalNameWarning)
warnings.filterwarnings('ignore', category=UserWarning)


def tune_me(pre_tune_hooks=None, post_tune_hooks=None, post_unpack_hooks=None):
    pre_tune_hooks = pre_tune_hooks or []
    post_tune_hooks = post_tune_hooks or []
    post_unpack_hooks = post_unpack_hooks or []

    def decorator(fnc_to_tune):

        def fnc(config):
            return fnc_to_tune(**config)

        fnc.__name__ = fnc_to_tune.__name__

        def run_tuning(*args, **kwargs):

            def clean_fixed_params(params):
                fixed_params = params['fixed_params']
                fixed_params.pop('seed', None)
                fixed_params.pop('verbosity', None)
                fixed_params.pop('ray_params', None)

            @parse_params(post_unpack_hooks=[clean_fixed_params, *post_unpack_hooks], use_verbosity=False)
            def main(search_alg, tune_params,
                     fixed_params, optimized_params, search_alg_params=None, ray_params=None):
                ray_params = ray_params or {}
                ray_params.setdefault('logging_level', 0)
                search_alg_params = search_alg_params or {}

                for param, data in optimized_params.items():
                    *prefixes, key = param.split('.')
                    curr_dict = fixed_params
                    for prefix in prefixes:
                        curr_dict = curr_dict[prefix]
                    curr_dict[key] = distribution_from_string(**data)

                settings = fixed_params

                # os.environ["TUNE_DISABLE_AUTO_CALLBACK_LOGGERS"] = "1"
                ray.init(**ray_params)

                for hook in pre_tune_hooks:
                    #  print(f'running: {hook.__name__}')
                    hook(settings)

                search_alg = search_alg_from_string(search_alg)(**search_alg_params)

                ray.tune.run(fnc,
                             search_alg=search_alg,
                             config=settings,
                             local_dir=None,
                             sync_config=tune.SyncConfig(syncer=None),
                             verbose=0,
                             name='tune',
                             max_failures=1,
                             **tune_params)

                ray.shutdown()

            main()

        return run_tuning

    return decorator


def fnc_with_params(fnc, params):
    if isinstance(params, list):
        return fnc(*params)
    elif isinstance(params, dict):
        return fnc(**params)
    elif isinstance(params, (int, float)):
        return fnc(params)


def grid_range(*args, **kwargs):
    return tune.grid_search(list(range(*args, **kwargs)))


def distribution_from_string(distribution, params):
    dct = {'uniform': tune.uniform,
           'quniform': tune.quniform,
           'loguniform': tune.loguniform,
           'qloguniform': tune.qloguniform,
           'randn': tune.randn,
           'qrandn': tune.qrandn,
           'randint': tune.randint,
           'lograndint': tune.lograndint,
           'qrandint': tune.qrandint,
           'qlograndint': tune.qlograndint,
           'choice': tune.choice,
           'func': tune.sample_from,
           'grid': tune.grid_search,
           'grid_range': grid_range}
    return fnc_with_params(dct[distribution], params)


def search_alg_from_string(alg):
    dct = {'BasicVariantGenerator': BasicVariantGenerator,
           'BayesOptSearch': BayesOptSearch}
    return dct[alg]
