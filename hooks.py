import pickle
from ast import literal_eval
from time import process_time
from writers import MongoRunWriter
import torch


def _rename_init_state(params, graph):
    if 'init_state' in params.keys():
        node = ((literal_eval(params['init_state']),), 0)
        params['init_state'] = graph.aug_node_map[node]


def determinize(model, writer, step=0, **params):
    _rename_init_state(params, model.graph)
    model.eval()
    strategy = model.strategy()
    delay = model.delay()

    start = process_time()
    schedule, vals = model.objective.determinize(
        strategy=strategy, delay=delay,
        **params
    )
    proc_time = process_time() - start
    writer.add_scalar('metrics.val_det', vals[-1])
    writer.add_scalar('metrics.schedule_len', len(schedule))
    writer.add_scalar('metrics.schedule_time', sum([l for _, l in schedule]))
    writer.add_scalar('times.determinize', proc_time)


def determinize_at(model, writer, step, run_every, **params):
    if step in run_every:
        determinize(model, writer, **params)


def determinize_at_best(model, writer, **params):
    _rename_init_state(params, model.graph)
    writer_view = writer.view()
    strategy = writer_view.get('strategy')
    delay = writer_view.get('delay')
    if isinstance(writer, MongoRunWriter):
        strategy = pickle.loads(strategy)
        delay = pickle.loads(delay)

    start = process_time()
    schedule, vals = model.objective.determinize(
        strategy=torch.from_numpy(strategy),
        delay=torch.from_numpy(delay),
        **params
    )
    proc_time = process_time() - start
    writer.add('metrics_stats.val_det_at_best', vals[-1])
    writer.add('metrics_stats.schedule_len_at_best', len(schedule))
    writer.add('metrics_stats.schedule_time_at_best', sum([l for _, l in schedule]))
    schedule = [((x, max(model.graph.graph.nodes[model.graph.nodes[x]]['rewards'])), y) for (x, y) in schedule]
    writer.add('metrics_stats.schedule_at_best', schedule)
    writer.add('times_stats.determinize_at_best', proc_time)
